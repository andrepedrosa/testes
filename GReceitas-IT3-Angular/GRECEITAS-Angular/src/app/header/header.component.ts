import { Component, OnInit, ChangeDetectorRef } from '@angular/core';

import { Subscription } from 'rxjs/Subscription';
import { AuthenticationService } from '../authentication/authentication.service';
import { User } from '../authentication/user.model';

@Component({
  selector: 'gr-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})


export class HeaderComponent implements OnInit {
  title = 'Welcome to My First Angular App!!';
  subscriptionAuth: Subscription;
  userInfo: User;
  constructor(
    private authenticationService: AuthenticationService,
    private cdr: ChangeDetectorRef) { }
  ngOnInit() {
    this.userInfo = this.authenticationService.userInfo;
    this.subscriptionAuth =
      this.authenticationService.authentication.subscribe((userInfo) => {
        this.userInfo = userInfo;
        this.cdr.detectChanges();
      });
  }
  ngOnDestroy() {
    this.subscriptionAuth.unsubscribe();
  }
}
