import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Receita } from './receita/receita.model'
import { ReceitasService } from './receitas.service'
import { AuthenticationService } from '../authentication/authentication.service';
import { User } from '../authentication/user.model';


@Component({
  selector: 'gr-receitas',
  templateUrl: './receitas.component.html',
  styleUrls: ['./receitas.component.css']
})
export class ReceitasComponent implements OnInit {

  rForm: FormGroup
  post: any
  //receitaId: string = ""


  receitas: Receita[]
  info: AuthenticationService

  constructor(private receitasService: ReceitasService,
    private aa: AuthenticationService,
    private fb: FormBuilder) {

    this.info = aa;
    this.rForm = fb.group({
      'receitaId': [null, Validators.required]


    })

  }

  ngOnInit() {

    console.log(this.info.userInfo);
    if (this.info.userInfo) {

      if (this.info.userInfo.medico) {
        console.log("XXXXX ENTROU")
        this.receitasService.getReceitasMedico(this.info.userInfo.id)
          .subscribe(
          receitas => { this.receitas = receitas },
          err => { console.log('Something went worng!') }
          )
      } else if (this.info.userInfo.utente && !this.info.userInfo.medico && !this.info.userInfo.farmaceutico) {
        this.receitasService.getReceitasPaciente(this.info.userInfo.id)
          .subscribe(
          receitas => { this.receitas = receitas },
          err => { console.log('Something went worng!') }
          )
      } else if (this.info.userInfo.farmaceutico) {
        
      }

    }
  }

  addPost(post){
    if(this.info.userInfo.farmaceutico){
      this.receitasService.getReceitaFarmaceutico(post.receitaId)
      .subscribe(
        receitas => {
          console.log(this.receitas)
          this.receitas = receitas},
        err => { console.log('Something went wrong!')}
      )
      //console.log("Receita Id: " + post.receitaId);
    }
    
  }

}