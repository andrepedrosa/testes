import { Injectable } from '@angular/core'
import { Observable } from 'rxjs/Observable'
import { HttpClient, HttpHeaders } from '@angular/common/http';
//import {ErrorHandler} from '../app.error-handler'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'


import { AuthenticationService } from '../authentication/authentication.service';

import { Receita } from './receita/receita.model'
import { GR_API } from '../app.api'

@Injectable()
export class ReceitasService {

    //private receitasUrl = 'http://localhost:3000/api/teste';

    constructor(private http: HttpClient,
        private authenticationService: AuthenticationService) { }
/*
    getReceitas(): Observable<Receita[]> {
        return this.http.get<Receita[]>(`${GR_API}/api/teste/`,this.getHeaders())
        //.catch(ErrorHandler.handleError)
    }
*/  

    getReceitasPaciente(id: string): Observable<Receita[]> {
        console.log(id);
        console.log(this.getHeaders());
        return this.http.get<Receita[]>(`${GR_API}/pacientes/receitas/${id}`,this.getHeaders())
        //.catch(ErrorHandler.handleError)
    }

    getReceitasMedico(id: string): Observable<Receita[]> {
        console.log(id);
        console.log(this.getHeaders());
        return this.http.get<Receita[]>(`${GR_API}/medicos/receitas/${id}`,this.getHeaders())
        //.catch(ErrorHandler.handleError)
    }

    
    getReceitaFarmaceutico(id: string): Observable<Receita[]> {
        console.log(id);
        console.log(this.getHeaders());
        return this.http.get<Receita[]>(`${GR_API}/farmaceuticos/receitas/${id}`,this.getHeaders())
        //.catch(ErrorHandler.handleError)
    }
    


    receitaById(id: string): Observable<Receita> {
        return this.http.get<Receita>(`${GR_API}/receitas/${id}`)
        //.catch(ErrorHandler.handleError)
    }

    getHeaders() {
        let headers = new HttpHeaders({
            'Authorization':
            this.authenticationService.userInfo.token
            
        });


        let httpOptions = {
            headers: headers
        };
        return httpOptions;
    }
}