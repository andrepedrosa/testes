import { Component, OnInit, Input } from '@angular/core';
import { Receita } from './receita.model'

@Component({
  selector: '[gr-receita]',
  templateUrl: './receita.component.html',
  styleUrls: ['./receita.component.css']
})
export class ReceitaComponent implements OnInit {

  @Input('gr-receita') receita: Receita

  constructor() { }

  ngOnInit() {
  }

}
