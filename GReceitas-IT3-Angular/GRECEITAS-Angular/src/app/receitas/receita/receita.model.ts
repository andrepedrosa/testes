export interface Receita {
    _id: string
    utente: string
    medico: string
    data: Date
    emitida: boolean
    nomeUtente: string
    nomeMedico: string
}