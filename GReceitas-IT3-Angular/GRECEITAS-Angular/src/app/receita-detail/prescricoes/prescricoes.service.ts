import {Injectable} from '@angular/core'
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs/Observable'
//import {ErrorHandler} from '../../app.error-handler'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'

import {Prescricao} from '../prescricoes/prescricao/prescricao.model'
import {GR_API} from '../../app.api'

@Injectable()
export class PrescricoesService { 

    constructor(private http:HttpClient){}

    prescricoes(id: string): Observable<Prescricao[]>{ 
        return this.http.get<Prescricao[]>(`${GR_API}/receitas/${id}/prescricao`)
          //.catch(ErrorHandler.handleError)
    }

}