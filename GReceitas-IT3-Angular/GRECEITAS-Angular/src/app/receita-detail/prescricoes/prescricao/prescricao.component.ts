import { Component, OnInit, Input } from '@angular/core';
import { Prescricao } from './prescricao.model'

@Component({
  selector: '[gr-prescricao]',
  templateUrl: './prescricao.component.html',
  styleUrls: ['./prescricao.component.css']
})
export class PrescricaoComponent implements OnInit {

  @Input('gr-prescricao') prescricao: Prescricao

  constructor() { }

  ngOnInit() {
  }

}