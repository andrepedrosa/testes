export interface Prescricao {
    _id: string
    apresentacao: string
    dataValidade: Date
    quantidade: number
}