import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Prescricao } from '../prescricoes/prescricao/prescricao.model'
import { PrescricoesService } from '../prescricoes/prescricoes.service'

@Component({
  selector: 'gr-prescricoes',
  templateUrl: './prescricoes.component.html',
  styleUrls: ['./prescricoes.component.css']
})
export class PrescricoesComponent implements OnInit {

  prescricoes: Prescricao[]

  constructor(private prescricoesService: PrescricoesService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.prescricoesService.prescricoes(this.route.snapshot.params['id'])
      .subscribe(prescricoes => this.prescricoes = prescricoes)
  }

}