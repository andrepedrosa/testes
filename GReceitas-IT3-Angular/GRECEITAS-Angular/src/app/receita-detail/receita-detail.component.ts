import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { Receita } from '../receitas/receita/receita.model'
import { ReceitasService } from '../receitas/receitas.service'

@Component({
  selector: 'gr-receita-detail',
  templateUrl: './receita-detail.component.html',
  styleUrls: ['./receita-detail.component.css']
})
export class ReceitaDetailComponent implements OnInit {

  receita: Receita

  constructor(private receitasService: ReceitasService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.receitasService.receitaById(this.route.snapshot.params['id'])
    .subscribe(receita => this.receita = receita)
  }

}
