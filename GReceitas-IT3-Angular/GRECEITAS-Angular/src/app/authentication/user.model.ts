export class User {
    id: string;
    token: string;
    tokenExp: number;
    medico: boolean;
    farmaceutico: boolean;
    utente: boolean;
    email: string;
    password: string;
    nome: string;
}