import { Component, OnInit } from '@angular/core'
import { Apresentacao } from './apresentacao/apresentacao.model'
import { ApresentacoesService } from './apresentacoes.service'
import { AuthenticationService } from '../authentication/authentication.service';
import { User } from '../authentication/user.model';

@Component({
  selector: 'gr-apresentacoes',
  templateUrl: './apresentacoes.component.html',
  styleUrls: ['./apresentacoes.component.css']
})
export class ApresentacoesComponent implements OnInit {

  apresentacoes: Apresentacao[]

  info: AuthenticationService

  constructor(private apresentacoesService: ApresentacoesService,
    private aa: AuthenticationService) {
    this.info = aa
  }

  ngOnInit() {
    this.apresentacoesService.apresentacoes()
      .subscribe(apresentacoes => {
        this.apresentacoes = apresentacoes
        console.log(apresentacoes)
      })

    if (this.info.userInfo) {
      if (this.info.userInfo.utente && !this.info.userInfo.medico && !this.info.userInfo.farmaceutico) {
        this.apresentacoesService.getApresentacoesPaciente(this.info.userInfo.id)
          .subscribe(
          apresentacoes => { this.apresentacoes = apresentacoes },
          err => {
            console.log('Something went worng!')
          })
      }
    }

  }

}