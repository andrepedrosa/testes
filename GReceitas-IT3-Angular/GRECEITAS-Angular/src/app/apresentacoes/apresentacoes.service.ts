import {Injectable} from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable'
//import {ErrorHandler} from '../app.error-handler'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'
import { AuthenticationService } from '../authentication/authentication.service';
import {Apresentacao} from './apresentacao/apresentacao.model'
import {GR_API} from '../app.api'

@Injectable()
export class ApresentacoesService { 

    constructor(private http:HttpClient,
    private authenticationService: AuthenticationService ){}

    apresentacoes(): Observable<Apresentacao[]>{
        return this.http.get<Apresentacao[]>(`${GR_API}/apresentacoes`)
          //.catch(ErrorHandler.handleError)
    }

    apresentacaoById(id: string): Observable<Apresentacao>{
        return this.http.get<Apresentacao>(`${GR_API}/apresentacoes/${id}`)
        //.catch(ErrorHandler.handleError)
    }


    getApresentacoesPaciente(id: string): Observable<Apresentacao[]> {
        console.log(id);
        console.log(this.getHeaders());
        return this.http.get<Apresentacao[]>(`${GR_API}/apresentacoes/pacientes/${id}`,this.getHeaders())
        //.catch(ErrorHandler.handleError)
    }

    getHeaders() {
        let headers = new HttpHeaders({
            'Authorization':
            this.authenticationService.userInfo.token
            
        });


        let httpOptions = {
            headers: headers
        };
        return httpOptions;
    }

}