import { Component, OnInit, Input } from '@angular/core';
import { Apresentacao } from './apresentacao.model'

@Component({
  selector: '[gr-apresentacao]',
  templateUrl: './apresentacao.component.html',
  styleUrls: ['./apresentacao.component.css']
})
export class ApresentacaoComponent implements OnInit {

  @Input('gr-apresentacao') apresentacao: Apresentacao

  constructor() { }

  ngOnInit() {
  }

}