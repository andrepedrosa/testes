export interface Apresentacao {
    _id: string
    medicamento: string
    farmaco: string
    posologia: string
    dosagem: string
    formato: string
    nomeMedicamento: string
    nomeFarmaco: string
}