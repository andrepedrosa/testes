import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosologiaComponent } from './posologia.component';

describe('PosologiaComponent', () => {
  let component: PosologiaComponent;
  let fixture: ComponentFixture<PosologiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosologiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosologiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
