import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosologiasComponent } from './posologias.component';

describe('PosologiasComponent', () => {
  let component: PosologiasComponent;
  let fixture: ComponentFixture<PosologiasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosologiasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosologiasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
