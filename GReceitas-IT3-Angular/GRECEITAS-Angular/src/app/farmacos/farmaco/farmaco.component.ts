import { Component, OnInit, Input } from '@angular/core';
import { Farmaco } from './farmaco.model'

@Component({
  selector: '[gr-farmaco]',
  templateUrl: './farmaco.component.html',
  styleUrls: ['./farmaco.component.css']
})
export class FarmacoComponent implements OnInit {

  @Input('gr-farmaco') farmaco: Farmaco

  constructor() { }

  ngOnInit() {
  }

}