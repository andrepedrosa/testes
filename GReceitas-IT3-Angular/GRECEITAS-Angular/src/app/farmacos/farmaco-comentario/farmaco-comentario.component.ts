import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { Farmaco } from '../farmaco/farmaco.model'
import { FarmacosService } from '../farmacos.service'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { AuthenticationService } from '../../authentication/authentication.service';
import { User } from '../../authentication/user.model';

@Component({
  selector: 'gr-farmaco-comentario',
  templateUrl: './farmaco-comentario.component.html',
  styleUrls: ['./farmaco-comentario.component.css']
})
export class FarmacoComentarioComponent implements OnInit {

  rForm: FormGroup
  post: any
  idMedico: string = ""
  comentario: string = ""

  farmaco: Farmaco

  info: AuthenticationService

  constructor(private farmacosService: FarmacosService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private aa:AuthenticationService) { 
      this.info = aa;
      this.rForm = fb.group({
        'comentario': [null, Validators.required]
      })

    }

  ngOnInit() {
    
    this.farmacosService.farmacoById(this.route.snapshot.params['id'])
      .subscribe(farmaco => this.farmaco = farmaco)
      
  }

  addComentario(post) {
    console.log("entrou no add comentario")

    this.farmacosService.addComentario(this.farmaco._id,
      this.info.userInfo.id,post.comentario)
      .subscribe()
  }


}