import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmacoComentarioComponent } from './farmaco-comentario.component';

describe('FarmacoComentarioComponent', () => {
  let component: FarmacoComentarioComponent;
  let fixture: ComponentFixture<FarmacoComentarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmacoComentarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmacoComentarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
