import { Component, OnInit } from '@angular/core';
import { Farmaco } from './farmaco/farmaco.model'
import { FarmacosService } from './farmacos.service'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'


@Component({
  selector: 'gr-farmacos',
  templateUrl: './farmacos.component.html',
  styleUrls: ['./farmacos.component.css']
})
export class FarmacosComponent implements OnInit {



  farmacos: Farmaco[]

  constructor(private farmacosService: FarmacosService) { }

  ngOnInit() {
    
    this.farmacosService.farmacos()
    .subscribe(farmacos => {
      this.farmacos = farmacos
      console.log(farmacos)
    })
    
  }



}