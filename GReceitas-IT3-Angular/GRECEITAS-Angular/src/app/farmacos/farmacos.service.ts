import {Injectable} from '@angular/core'
import {Observable} from 'rxjs/Observable'
//import {ErrorHandler} from '../app.error-handler'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'
import { HttpClient, HttpHeaders, HttpErrorResponse } from
'@angular/common/http';
import {Farmaco} from './farmaco/farmaco.model'
import {GR_API} from '../app.api'
import { AuthenticationService } from '../authentication/authentication.service';


@Injectable()
export class FarmacosService { 

    constructor(private http:HttpClient,
    private authenticationService : AuthenticationService){

    }

    farmacos(): Observable<Farmaco[]>{
        return this.http.get<Farmaco[]>(`${GR_API}/farmacos`)
          //.catch(ErrorHandler.handleError)
    }

    farmacoById(id: string): Observable<Farmaco>{
        return this.http.get<Farmaco>(`${GR_API}/farmacos/${id}`)
        //.catch(ErrorHandler.handleError)
    }

    addComentario(id: string, medicoId: string, comentario: string): Observable<boolean> {
        console.log("entrou no addComenterio service")
        console.log("medicoId: "+medicoId)
        return new Observable<boolean>(observer => {
            this.http.post<Farmaco>(`${GR_API}/farmacos/${id}/comentarios`,{
                medico: medicoId,
                comentario: comentario,
            },this.getHeaders())
                .subscribe(data => {
                    console.log("comentario criado")
                },
                (err: HttpErrorResponse) => {
                    if (err.error instanceof Error) {
                        console.log("Client-side error occured.");
                    } else {
                        console.log("Server-side error occured.");
                    }
                    console.log(err);
                    observer.next(false);
                });

        });
    }



    getHeaders() {
        let headers = new HttpHeaders({
            'Authorization':
            this.authenticationService.userInfo.token
            
        });
    
    
        let httpOptions = {
            headers: headers
        };
        return httpOptions;
    }
    


}