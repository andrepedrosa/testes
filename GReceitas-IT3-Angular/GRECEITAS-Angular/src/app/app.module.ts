import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms'

import {ROUTES} from './app.routes'

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ReceitasComponent } from './receitas/receitas.component';
import { ReceitaComponent } from './receitas/receita/receita.component'

import { ReceitasService } from './receitas/receitas.service';
import { ReceitaDetailComponent } from './receita-detail/receita-detail.component';
import { PrescricaoComponent } from './receita-detail/prescricoes/prescricao/prescricao.component';
import { PrescricoesComponent } from './receita-detail/prescricoes/prescricoes.component';
import { PrescricoesService } from './receita-detail/prescricoes/prescricoes.service';
import { ApresentacoesService } from './apresentacoes/apresentacoes.service';
import { ApresentacoesComponent } from './apresentacoes/apresentacoes.component';
import { ApresentacaoComponent } from './apresentacoes/apresentacao/apresentacao.component';
import { InputComponent } from './shared/input/input.component';
import { AuthenticationService } from './authentication/authentication.service';
import { LoginComponent } from './authentication/login/login.component';
import { AuthGuard } from './authentication/guards/auth.guard';
import { MedicoGuard } from './authentication/guards/medico.guard';
import { UtenteGuard } from './authentication/guards/utente.guard';
import { FarmaceuticoGuard } from './authentication/guards/farmaceutico.guard';
import { SignupComponent } from './authentication/signup/signup.component';
import { FarmacosComponent } from './farmacos/farmacos.component';
import { FarmacoComponent } from './farmacos/farmaco/farmaco.component';
import { FarmacoComentarioComponent } from './farmacos/farmaco-comentario/farmaco-comentario.component';
import { FarmacosService } from './farmacos/farmacos.service';
import { PosologiasComponent } from './posologias/posologias.component';
import { PosologiaComponent } from './posologias/posologia/posologia.component'


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    AboutComponent,
    ReceitasComponent,
    ReceitaComponent,
    ReceitaDetailComponent,
    PrescricaoComponent,
    PrescricoesComponent,
    ApresentacoesComponent,
    ApresentacaoComponent,
    InputComponent,
    LoginComponent,
    SignupComponent,
    FarmacosComponent,
    FarmacoComponent,
    FarmacoComentarioComponent,
    PosologiasComponent,
    PosologiaComponent,

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [
    AuthGuard,
    MedicoGuard,
    UtenteGuard,
    FarmaceuticoGuard,
    AuthenticationService,
    ReceitasService,
    PrescricoesService,
    ApresentacoesService,
    FarmacosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
