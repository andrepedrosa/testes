import {Component, OnInit} from "@angular/core"

@Component({
  selector: 'gr-app',
  templateUrl: 'app.component.html'
})
export class AppComponent implements OnInit {

  content = 'Welcome do GR App!'

  constructor() { }

  ngOnInit() {
  }

}