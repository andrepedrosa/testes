import { Routes } from '@angular/router'

import { HomeComponent } from './home/home.component'
import { AboutComponent } from './about/about.component'
import { ReceitasComponent } from './receitas/receitas.component'
import { ReceitaComponent } from './receitas//receita/receita.component'
import { ReceitaDetailComponent } from './receita-detail/receita-detail.component'
import { PrescricoesComponent } from './receita-detail/prescricoes/prescricoes.component'
import { ApresentacoesComponent } from './apresentacoes/apresentacoes.component'
import { FarmacosComponent } from './farmacos/farmacos.component'
import { LoginComponent} from './authentication/login/login.component'
import { SignupComponent} from './authentication/signup/signup.component'
import { AuthGuard } from './authentication/guards/auth.guard';
import { MedicoGuard } from './authentication/guards/medico.guard';
import { UtenteGuard } from './authentication/guards/utente.guard';
import { FarmaceuticoGuard } from './authentication/guards/farmaceutico.guard';
import { FarmacoComentarioComponent } from './farmacos/farmaco-comentario/farmaco-comentario.component';




export const ROUTES: Routes = [
    { path: '', component: HomeComponent },
    { path: 'login', component: LoginComponent},
    { path: 'signup', component: SignupComponent},
    { path: 'about', component: AboutComponent },
    { path: 'receitas', component: ReceitasComponent, canActivate: [AuthGuard, FarmaceuticoGuard]},
    { path: 'receitas/receitaid', component: ReceitasComponent, canActivate: [AuthGuard, FarmaceuticoGuard]},
    { path: 'receitas/medicoid', component: ReceitasComponent, canActivate: [AuthGuard, MedicoGuard]}, 
    { path: 'receitas/pacienteid', component: ReceitasComponent, canActivate: [AuthGuard, UtenteGuard]},
    { path: 'receitas/:id', component: ReceitaDetailComponent },
    { path: 'receitas/:id', component: PrescricoesComponent },
    { path: 'apresentacoes', component: ApresentacoesComponent },
    { path: 'farmacos', component: FarmacosComponent },
    { path: 'farmacos/:id', component: FarmacoComentarioComponent },
    { path: 'apresentacoes/pacientes/pacienteId', component: ApresentacoesComponent, canActivate: [AuthGuard, UtenteGuard]},
]
