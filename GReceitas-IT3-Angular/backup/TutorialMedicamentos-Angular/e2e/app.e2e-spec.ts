'use strict'; // necessary for es6 output in node

import { browser, element, by, ElementFinder, ElementArrayFinder } from 'protractor';
import { promise } from 'selenium-webdriver';

const expectedH1 = 'Tour of Medicamentos';
const expectedTitle = `${expectedH1}`;
const targetMedicamento = { id: 15, name: 'Magneta' };
const targetMedicamentoDashboardIndex = 3;
const nameSuffix = 'X';
const newMedicamentoName = targetMedicamento.name + nameSuffix;

class Medicamento {
  id: number;
  name: string;

  // Factory methods

  // Medicamento from string formatted as '<id> <name>'.
  static fromString(s: string): Medicamento {
    return {
      id: +s.substr(0, s.indexOf(' ')),
      name: s.substr(s.indexOf(' ') + 1),
    };
  }

  // Medicamento from medicamento list <li> element.
  static async fromLi(li: ElementFinder): Promise<Medicamento> {
      let stringsFromA = await li.all(by.css('a')).getText();
      let strings = stringsFromA[0].split(' ');
      return { id: +strings[0], name: strings[1] };
  }

  // Medicamento id and name from the given detail element.
  static async fromDetail(detail: ElementFinder): Promise<Medicamento> {
    // Get medicamento id from the first <div>
    let _id = await detail.all(by.css('div')).first().getText();
    // Get name from the h2
    let _name = await detail.element(by.css('h2')).getText();
    return {
        id: +_id.substr(_id.indexOf(' ') + 1),
        name: _name.substr(0, _name.lastIndexOf(' '))
    };
  }
}

describe('Tutorial part 6', () => {

  beforeAll(() => browser.get(''));

  function getPageElts() {
    let navElts = element.all(by.css('app-root nav a'));

    return {
      navElts: navElts,

      appDashboardHref: navElts.get(0),
      appDashboard: element(by.css('app-root app-dashboard')),
      topMedicamentos: element.all(by.css('app-root app-dashboard > div h4')),

      appMedicamentosHref: navElts.get(1),
      appMedicamentos: element(by.css('app-root app-medicamentos')),
      allMedicamentos: element.all(by.css('app-root app-medicamentos li')),
      selectedMedicamentoSubview: element(by.css('app-root app-medicamentos > div:last-child')),

      medicamentoDetail: element(by.css('app-root app-medicamento-detail > div')),

      searchBox: element(by.css('#search-box')),
      searchResults: element.all(by.css('.search-result li'))
    };
  }

  describe('Initial page', () => {

    it(`has title '${expectedTitle}'`, () => {
      expect(browser.getTitle()).toEqual(expectedTitle);
    });

    it(`has h1 '${expectedH1}'`, () => {
        expectHeading(1, expectedH1);
    });

    const expectedViewNames = ['Dashboard', 'Medicamentos'];
    it(`has views ${expectedViewNames}`, () => {
      let viewNames = getPageElts().navElts.map((el: ElementFinder) => el.getText());
      expect(viewNames).toEqual(expectedViewNames);
    });

    it('has dashboard as the active view', () => {
      let page = getPageElts();
      expect(page.appDashboard.isPresent()).toBeTruthy();
    });

  });

  describe('Dashboard tests', () => {

    beforeAll(() => browser.get(''));

    it('has top medicamentos', () => {
      let page = getPageElts();
      expect(page.topMedicamentos.count()).toEqual(4);
    });

    it(`selects and routes to ${targetMedicamento.name} details`, dashboardSelectTargetMedicamento);

    it(`updates medicamento name (${newMedicamentoName}) in details view`, updateMedicamentoNameInDetailView);

    it(`cancels and shows ${targetMedicamento.name} in Dashboard`, () => {
      element(by.buttonText('go back')).click();
      browser.waitForAngular(); // seems necessary to gets tests to pass for toh-pt6

      let targetMedicamentoElt = getPageElts().topMedicamentos.get(targetMedicamentoDashboardIndex);
      expect(targetMedicamentoElt.getText()).toEqual(targetMedicamento.name);
    });

    it(`selects and routes to ${targetMedicamento.name} details`, dashboardSelectTargetMedicamento);

    it(`updates medicamento name (${newMedicamentoName}) in details view`, updateMedicamentoNameInDetailView);

    it(`saves and shows ${newMedicamentoName} in Dashboard`, () => {
      element(by.buttonText('save')).click();
      browser.waitForAngular(); // seems necessary to gets tests to pass for toh-pt6

      let targetMedicamentoElt = getPageElts().topMedicamentos.get(targetMedicamentoDashboardIndex);
      expect(targetMedicamentoElt.getText()).toEqual(newMedicamentoName);
    });

  });

  describe('Medicamentos tests', () => {

    beforeAll(() => browser.get(''));

    it('can switch to Medicamentos view', () => {
      getPageElts().appMedicamentosHref.click();
      let page = getPageElts();
      expect(page.appMedicamentos.isPresent()).toBeTruthy();
      expect(page.allMedicamentos.count()).toEqual(10, 'number of medicamentos');
    });

    it('can route to medicamento details', async () => {
      getMedicamentoLiEltById(targetMedicamento.id).click();

      let page = getPageElts();
      expect(page.medicamentoDetail.isPresent()).toBeTruthy('shows medicamento detail');
      let medicamento = await Medicamento.fromDetail(page.medicamentoDetail);
      expect(medicamento.id).toEqual(targetMedicamento.id);
      expect(medicamento.name).toEqual(targetMedicamento.name.toUpperCase());
    });

    it(`updates medicamento name (${newMedicamentoName}) in details view`, updateMedicamentoNameInDetailView);

    it(`shows ${newMedicamentoName} in Medicamentos list`, () => {
      element(by.buttonText('save')).click();
      browser.waitForAngular();
      let expectedText = `${targetMedicamento.id} ${newMedicamentoName}`;
      expect(getMedicamentoAEltById(targetMedicamento.id).getText()).toEqual(expectedText);
    });

    it(`deletes ${newMedicamentoName} from Medicamentos list`, async () => {
      const medicamentosBefore = await toMedicamentoArray(getPageElts().allMedicamentos);
      const li = getMedicamentoLiEltById(targetMedicamento.id);
      li.element(by.buttonText('x')).click();

      const page = getPageElts();
      expect(page.appMedicamentos.isPresent()).toBeTruthy();
      expect(page.allMedicamentos.count()).toEqual(9, 'number of medicamentos');
      const medicamentosAfter = await toMedicamentoArray(page.allMedicamentos);
      // console.log(await Medicamento.fromLi(page.allMedicamentos[0]));
      const expectedMedicamentos =  medicamentosBefore.filter(h => h.name !== newMedicamentoName);
      expect(medicamentosAfter).toEqual(expectedMedicamentos);
      // expect(page.selectedMedicamentoSubview.isPresent()).toBeFalsy();
    });

    it(`adds back ${targetMedicamento.name}`, async () => {
      const newMedicamentoName = 'Alice';
      const medicamentosBefore = await toMedicamentoArray(getPageElts().allMedicamentos);
      const numMedicamentos = medicamentosBefore.length;

      element(by.css('input')).sendKeys(newMedicamentoName);
      element(by.buttonText('add')).click();

      let page = getPageElts();
      let medicamentosAfter = await toMedicamentoArray(page.allMedicamentos);
      expect(medicamentosAfter.length).toEqual(numMedicamentos + 1, 'number of medicamentos');

      expect(medicamentosAfter.slice(0, numMedicamentos)).toEqual(medicamentosBefore, 'Old medicamentos are still there');

      const maxId = medicamentosBefore[medicamentosBefore.length - 1].id;
      expect(medicamentosAfter[numMedicamentos]).toEqual({id: maxId + 1, name: newMedicamentoName});
    });
  });

  describe('Progressive medicamento search', () => {

    beforeAll(() => browser.get(''));

    it(`searches for 'Ma'`, async () => {
      getPageElts().searchBox.sendKeys('Ma');
      browser.sleep(1000);

      expect(getPageElts().searchResults.count()).toBe(4);
    });

    it(`continues search with 'g'`, async () => {
      getPageElts().searchBox.sendKeys('g');
      browser.sleep(1000);
      expect(getPageElts().searchResults.count()).toBe(2);
    });

    it(`continues search with 'e' and gets ${targetMedicamento.name}`, async () => {
      getPageElts().searchBox.sendKeys('n');
      browser.sleep(1000);
      let page = getPageElts();
      expect(page.searchResults.count()).toBe(1);
      let medicamento = page.searchResults.get(0);
      expect(medicamento.getText()).toEqual(targetMedicamento.name);
    });

    it(`navigates to ${targetMedicamento.name} details view`, async () => {
      let medicamento = getPageElts().searchResults.get(0);
      expect(medicamento.getText()).toEqual(targetMedicamento.name);
      medicamento.click();

      let page = getPageElts();
      expect(page.medicamentoDetail.isPresent()).toBeTruthy('shows medicamento detail');
      let medicamento2 = await Medicamento.fromDetail(page.medicamentoDetail);
      expect(medicamento2.id).toEqual(targetMedicamento.id);
      expect(medicamento2.name).toEqual(targetMedicamento.name.toUpperCase());
    });
  });

  async function dashboardSelectTargetMedicamento() {
    let targetMedicamentoElt = getPageElts().topMedicamentos.get(targetMedicamentoDashboardIndex);
    expect(targetMedicamentoElt.getText()).toEqual(targetMedicamento.name);
    targetMedicamentoElt.click();
    browser.waitForAngular(); // seems necessary to gets tests to pass for toh-pt6

    let page = getPageElts();
    expect(page.medicamentoDetail.isPresent()).toBeTruthy('shows medicamento detail');
    let medicamento = await Medicamento.fromDetail(page.medicamentoDetail);
    expect(medicamento.id).toEqual(targetMedicamento.id);
    expect(medicamento.name).toEqual(targetMedicamento.name.toUpperCase());
  }

  async function updateMedicamentoNameInDetailView() {
    // Assumes that the current view is the medicamento details view.
    addToMedicamentoName(nameSuffix);

    let page = getPageElts();
    let medicamento = await Medicamento.fromDetail(page.medicamentoDetail);
    expect(medicamento.id).toEqual(targetMedicamento.id);
    expect(medicamento.name).toEqual(newMedicamentoName.toUpperCase());
  }

});

function addToMedicamentoName(text: string): promise.Promise<void> {
  let input = element(by.css('input'));
  return input.sendKeys(text);
}

function expectHeading(hLevel: number, expectedText: string): void {
    let hTag = `h${hLevel}`;
    let hText = element(by.css(hTag)).getText();
    expect(hText).toEqual(expectedText, hTag);
};

function getMedicamentoAEltById(id: number): ElementFinder {
  let spanForId = element(by.cssContainingText('li span.badge', id.toString()));
  return spanForId.element(by.xpath('..'));
}

function getMedicamentoLiEltById(id: number): ElementFinder {
  let spanForId = element(by.cssContainingText('li span.badge', id.toString()));
  return spanForId.element(by.xpath('../..'));
}

async function toMedicamentoArray(allMedicamentos: ElementArrayFinder): Promise<Medicamento[]> {
  let promisedMedicamentos = await allMedicamentos.map(Medicamento.fromLi);
  // The cast is necessary to get around issuing with the signature of Promise.all()
  return <Promise<any>> Promise.all(promisedMedicamentos);
}
