import { Component, OnInit } from '@angular/core';

import { Medicamento } from '../medicamento';
import { MedicamentoService } from '../medicamento.service';

@Component({
  selector: 'app-medicamentos',
  templateUrl: './medicamentos.component.html',
  styleUrls: ['./medicamentos.component.css']
})
export class MedicamentosComponent implements OnInit {
  medicamentos: Medicamento[];

  constructor(private medicamentoService: MedicamentoService) { }

  ngOnInit() {
    this.getMedicamentos();
  }

  getMedicamentos(): void {
    this.medicamentoService.getMedicamentos()
    .subscribe(medicamentos => this.medicamentos = medicamentos);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.medicamentoService.addMedicamento({ name } as Medicamento)
      .subscribe(medicamento => {
        this.medicamentos.push(medicamento);
      });
  }

  delete(medicamento: Medicamento): void {
    this.medicamentos = this.medicamentos.filter(h => h !== medicamento);
    this.medicamentoService.deleteMedicamento(medicamento).subscribe();
  }

}
