import { Medicamento } from './medicamento';

export const HEROES: Medicamento[] = [
  { id: 11, name: 'Aspegic' },
  { id: 12, name: 'Atarax' },
  { id: 13, name: 'Benuron' },
  { id: 14, name: 'Voltaren' },
  { id: 15, name: 'Apirina' },
  { id: 16, name: 'Anti gripe X' },
  { id: 17, name: 'Victan Plus' },
  { id: 18, name: 'Desloratadina' },
  { id: 19, name: 'Brufen' }
];
