import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { Medicamento } from './medicamento';
import { MessageService } from './message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class MedicamentoService {

  private medicamentosUrl = 'api/medicamentos';  // URL to web api

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  /** GET medicamentos from the server */
  getMedicamentos (): Observable<Medicamento[]> {
    return this.http.get<Medicamento[]>(this.medicamentosUrl)
      .pipe(
        tap(medicamentos => this.log(`fetched medicamentos`)),
        catchError(this.handleError('getMedicamentos', []))
      );
  }

  /** GET medicamento by id. Return `undefined` when id not found */
  getMedicamentoNo404<Data>(id: number): Observable<Medicamento> {
    const url = `${this.medicamentosUrl}/?id=${id}`;
    return this.http.get<Medicamento[]>(url)
      .pipe(
        map(medicamentos => medicamentos[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} medicamento id=${id}`);
        }),
        catchError(this.handleError<Medicamento>(`getMedicamento id=${id}`))
      );
  }

  /** GET medicamento by id. Will 404 if id not found */
  getMedicamento(id: number): Observable<Medicamento> {
    const url = `${this.medicamentosUrl}/${id}`;
    return this.http.get<Medicamento>(url).pipe(
      tap(_ => this.log(`fetched medicamento id=${id}`)),
      catchError(this.handleError<Medicamento>(`getMedicamento id=${id}`))
    );
  }

  /* GET medicamentos whose name contains search term */
  searchMedicamentos(term: string): Observable<Medicamento[]> {
    if (!term.trim()) {
      // if not search term, return empty medicamento array.
      return of([]);
    }
    return this.http.get<Medicamento[]>(`api/medicamentos/?name=${term}`).pipe(
      tap(_ => this.log(`found medicamentos matching "${term}"`)),
      catchError(this.handleError<Medicamento[]>('searchMedicamentos', []))
    );
  }

  //////// Save methods //////////

  /** POST: add a new medicamento to the server */
  addMedicamento (medicamento: Medicamento): Observable<Medicamento> {
    return this.http.post<Medicamento>(this.medicamentosUrl, medicamento, httpOptions).pipe(
      tap((medicamento: Medicamento) => this.log(`added medicamento w/ id=${medicamento.id}`)),
      catchError(this.handleError<Medicamento>('addMedicamento'))
    );
  }

  /** DELETE: delete the medicamento from the server */
  deleteMedicamento (medicamento: Medicamento | number): Observable<Medicamento> {
    const id = typeof medicamento === 'number' ? medicamento : medicamento.id;
    const url = `${this.medicamentosUrl}/${id}`;

    return this.http.delete<Medicamento>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted medicamento id=${id}`)),
      catchError(this.handleError<Medicamento>('deleteMedicamento'))
    );
  }

  /** PUT: update the medicamento on the server */
  updateMedicamento (medicamento: Medicamento): Observable<any> {
    return this.http.put(this.medicamentosUrl, medicamento, httpOptions).pipe(
      tap(_ => this.log(`updated medicamento id=${medicamento.id}`)),
      catchError(this.handleError<any>('updateMedicamento'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a MedicamentoService message with the MessageService */
  private log(message: string) {
    this.messageService.add('MedicamentoService: ' + message);
  }
}
