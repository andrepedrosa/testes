import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Medicamento }         from '../medicamento';
import { MedicamentoService }  from '../medicamento.service';

@Component({
  selector: 'app-medicamento-detail',
  templateUrl: './medicamento-detail.component.html',
  styleUrls: [ './medicamento-detail.component.css' ]
})
export class MedicamentoDetailComponent implements OnInit {
  @Input() medicamento: Medicamento;

  constructor(
    private route: ActivatedRoute,
    private medicamentoService: MedicamentoService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getMedicamento();
  }

  getMedicamento(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.medicamentoService.getMedicamento(id)
      .subscribe(medicamento => this.medicamento = medicamento);
  }

  goBack(): void {
    this.location.back();
  }

 save(): void {
    this.medicamentoService.updateMedicamento(this.medicamento)
      .subscribe(() => this.goBack());
  }
}
