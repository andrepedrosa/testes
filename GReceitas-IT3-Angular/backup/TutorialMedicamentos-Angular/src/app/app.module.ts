import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

import { AppRoutingModule }     from './app-routing.module';

import { AppComponent }         from './app.component';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { MedicamentoDetailComponent }  from './medicamento-detail/medicamento-detail.component';
import { MedicamentosComponent }      from './medicamentos/medicamentos.component';
import { MedicamentoSearchComponent }  from './medicamento-search/medicamento-search.component';
import { MedicamentoService }          from './medicamento.service';
import { MessageService }       from './message.service';
import { MessagesComponent }    from './messages/messages.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,

    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    // Remove it when a real server is ready to receive requests.
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    MedicamentosComponent,
    MedicamentoDetailComponent,
    MessagesComponent,
    MedicamentoSearchComponent
  ],
  providers: [ MedicamentoService, MessageService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
