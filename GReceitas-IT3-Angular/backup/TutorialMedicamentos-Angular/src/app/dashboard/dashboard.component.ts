import { Component, OnInit } from '@angular/core';
import { Medicamento } from '../medicamento';
import { MedicamentoService } from '../medicamento.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {
  medicamentos: Medicamento[] = [];

  constructor(private medicamentoService: MedicamentoService) { }

  ngOnInit() {
    this.getMedicamentos();
  }

  getMedicamentos(): void {
    this.medicamentoService.getMedicamentos()
      .subscribe(medicamentos => this.medicamentos = medicamentos.slice(1, 5));
  }
}
