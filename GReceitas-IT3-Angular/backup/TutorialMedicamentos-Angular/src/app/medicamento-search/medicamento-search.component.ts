import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Subject }    from 'rxjs/Subject';
import { of }         from 'rxjs/observable/of';

import {
   debounceTime, distinctUntilChanged, switchMap
 } from 'rxjs/operators';

import { Medicamento } from '../medicamento';
import { MedicamentoService } from '../medicamento.service';

@Component({
  selector: 'app-medicamento-search',
  templateUrl: './medicamento-search.component.html',
  styleUrls: [ './medicamento-search.component.css' ]
})
export class MedicamentoSearchComponent implements OnInit {
  medicamentos$: Observable<Medicamento[]>;
  private searchTerms = new Subject<string>();

  constructor(private medicamentoService: MedicamentoService) {}

  // Push a search term into the observable stream.
  search(term: string): void {
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.medicamentos$ = this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),

      // ignore new term if same as previous term
      distinctUntilChanged(),

      // switch to new search observable each time the term changes
      switchMap((term: string) => this.medicamentoService.searchMedicamentos(term)),
    );
  }
}
