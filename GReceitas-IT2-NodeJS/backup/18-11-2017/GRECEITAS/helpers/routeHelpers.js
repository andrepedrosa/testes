const Joi = require('joi');

module.exports = {
    validateBody: function(schema){
        return function(req,res,next){
            const result = Joi.validate(req.body,schema);
            if(result.error){
                return res.status(400).json(result.error);
            }
            if(!req.value){ req.value = {}; }
            req.value['body'] = result.value;
            next();
        }
    },

    schemas: {
        authSchema: Joi.object().keys({
            nome: Joi.string().required(),
            email: Joi.string().email().required(),
            password: Joi.string().required().regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}/),
            medico: Joi.boolean().required(),
            utente: Joi.boolean().required().default(true),
            farmaceutico: Joi.boolean().required()
        })
    }
}