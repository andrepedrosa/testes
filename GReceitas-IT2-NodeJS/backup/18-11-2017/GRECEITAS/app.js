const express = require('express');
const logger = require('morgan');
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const http = require('http');
const path = require('path');

const port = 3000;


mongoose.Promise = global.Promise;
//mongoose.connect('mongodb://localhost/apiprojecttest', { useMongoClient: true });
var uri = 'mongodb://1071205:greceitas2017@ds237475.mlab.com:37475/greceitas';
db = mongoose.connect(uri);
mongoose.set('debug', true);

const app =  express();


//View engine
app.set('views', path.join(__dirname,views));
app.set('view engine', 'ejs');
app.engine('html',require('ejs').renderFile);

//Routes
const receitas = require('./routes/receitas')
const users = require('./routes/users')

//Middlewares
app.use(logger('combined'));
app.use(bodyParser.json());

//Routes
app.use('/users', users);
app.use('/receitas', receitas);

//Catch 404 errors and foward them to error handler
app.use(function(req,res,next){
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

//Error handler function
app.use(function(err,req,res,next){
    const error = app.get('env') === 'development' ? err : {};
    const status = err.status || 500;
    //Respond to client
    res.status(status).json({
        error:{
            message: error.message
        }
    });
    //Respond to ourselves
    console.error(err);
});


//Start the server
app.listen(port, (err) => {
    console.log(`running server on port: ${port}`);
});