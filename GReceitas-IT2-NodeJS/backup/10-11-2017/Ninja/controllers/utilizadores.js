const JWT = require('jsonwebtoken');
const Utilizador = require('../models/utilizador');
const { JWT_SECRET } = require('../configuration');
const Receita = require('../models/receita');

signToken = function(utilizador){
    return JWT.sign({
        iss: 'GReceitas',
        //sub: utilizador.id,
        iat: new Date().getTime(),
        //exp: new Date().setDate(new Date().getTime() + 2*60000)
        //(new Date().getTime() + 1*60000)
        //data de expiracao + 1 dia apos data do sign
        exp: new Date().setDate(new Date().getDate()+1)
    }, JWT_SECRET);
}


module.exports = {

    //get all with Async / Await
    
    index: async function(req, res, next){
        try{
            const utilizadores = await Utilizador.find({});
            res.status(200).json(utilizadores);
        }
        //existe um modulo que se chama express-promise-route que pode ser instalado e que deixa se ser necessário o try catch
        //https://www.youtube.com/watch?v=9tLA4r-gqds
        //minuto 14
        catch(err){
            next(err);
        }  
    },
    

    //Queries by ID
    // Get by ID using async / await
    getUtilizadorById: async function(req,res,next){
        try{
            const {utilizadorId} = req.params;
            //equivalente a:
            //const utilizadorId = req.params.utilizadorId;
            //console.log("req.params.utilizadorId", utilizadorId);
            const utilizador = await Utilizador.findById(utilizadorId);
            res.status(200).json(utilizador);
        }
        catch(err){
            next(err);
        }    
    },
    

    //Put an utilizador by id
    replaceUtilizadorById: async function(req,res,next){
        try{
            const {utilizadorId} = req.params;
            const newUtilizador = req.body;
            const result = await Utilizador.findByIdAndUpdate(utilizadorId,newUtilizador);
            const utilizador = await Utilizador.findById(utilizadorId);
            res.status(200).json(utilizador);
        }
        catch(err){
            next(err);
        }
    },

    //Get Utilizador Receitas
    getUtilizadorReceitas: async function(req,res,next){
        try{
            const {utilizadorId} = req.params;
            const utilizador = await Utilizador.findById(utilizadorId).populate('receitas');
            res.status(200).json(utilizador.receitas);
        }
        catch(err){
            next(err);
        }
    },


    //Signup new user
    //router.route('/signup')
    //.post(validateBody(schemas.authSchema), UtilizadoresController.signUp);
    signUp: async function(req,res,next){
        try{
            
            const nome = req.value.body.nome;
            //const email = req.body["email"];
            //const password = req.value.body.password;
            //const utente = req.value.body.utente;
            //const medico = req.value.body.medico;
            //const farmaceutico = req.value.body.farmaceutico;

            console.log("email", nome);


            
            //check is user already exists
            const foundUser = await User.findOne({ email: email});
            if(foundUser){
                //403 - forbiden
                return res.status(403).json({ error: "Email already in use"});
            }

            //create new user
            const newUser = new User({
                nome: nome,
                email: email,
                password: password
            });
            await newUser.save();

            //generate the token
            const token = signToken(newUser);

            //respond with token
            res.status(200).json({ token: token});
            

        }catch(err){
            next(err);
        }
    },

    signIn: async function(req,res,next){
        try{
            //console.log("Successful login!")
            //generate a token

            const token = signToken(req.user);
            res.status(200).json({token: token});
        }catch(err){
            next(err);
        }
    },

    secret: async function(req,res,next){
        try{
            console.log('I managed to get here!');
            res.json({ secret: "resource" });
        }catch(err){
            next(err);
        }
    }


  
}
/*
We can interact with mongoose in 3 different ways:
1) Callbacks - worst way
2) Promises
3) Async / Await (Promises) - best way
*/