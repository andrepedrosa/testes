const JWT = require('jsonwebtoken');
const User = require('../models/user');
const { JWT_SECRET } = require('../configuration');
const bcrypt = require('bcrypt');



signToken = function(u){
    console.log("U: ",u);
    return JWT.sign({
        iss: 'GReceitas',
        sub: u.id_,
        iat: new Date().getTime(),
        //exp: new Date().setDate(new Date().getTime() + 2*60000)
        //(new Date().getTime() + 1*60000)
        //data de expiracao + 1 dia apos data do sign
        exp: new Date().setDate(new Date().getDate()+1)
    }, JWT_SECRET);
}


module.exports = {
    index: async function(req,res,next){
        try{
            const users = await User.find({});
            res.status(200).json(users);
        }catch(err){
            next(err);
        }
    },
    signUp: async function(req,res,next){
        try{
            const nome = req.body["nome"];
            const email = req.body["email"];
            const password = req.body["password"];
            const medico = req.body["medico"];
            const utente = req.body["utente"];
            const farmaceutico = req.body["farmaceutico"];

            
            //check is user already exists
            const foundUser = await User.findOne({ email: email});
            if(foundUser){
                //403 - forbiden
                return res.status(403).json({ error: "Email already in use"});
            }
            

            //create new user
            var newUser = new User();
            newUser.nome = nome;
            newUser.email = email;
            newUser.password = password;
            newUser.medico = medico;
            newUser.utente = utente;
            newUser.farmaceutico = farmaceutico;

            console.log("newuser: ", newUser);
            await newUser.save();

            //generate the token
            //const token = signToken(newUser);

            //respond with token
            res.status(200).json(newUser);
            

        }catch(err){
            next(err);
        }
    },

    signIn: async function(req,res,next){
        try{
            const password = req.body.password;
            console.log("req.body.password: ",req.body.password);
            console.log("req.body.email: ",req.body.email);
            user = await User.findOne({email: req.body.email});
            console.log("user email:", user.email);
            console.log(user.password);
            console.log(req.user);

 
            if (!user) {
                res.json({ success: false, message: 'Authentication failed. User not found.' });
            } else if (user) {
        
                // check if password matches

                bcrypt.compare(password, user.password, function(err, resposta) {
                    if(resposta) {
                        // Passwords match
                        //console.log("Successful login!")
                        //generate a token
                        console.log("YESSSSSSS");
                        const token = signToken(user);
                
                        // return the information including token as JSON
                        res.status(200).json({
                            success: true,
                            message: 'Enjoy your token!',
                            token: token
                        });
                    } else {
                        // Passwords don't match
                        res.json({ success: false, message: 'Authentication failed. Wrong password.' });
                    } 
                  });
                
            }
                     
        }catch(err){
            next(err);
        }
    },




    secret: async function(req,res,next){
        try{
            console.log('I managed to get here!');
            res.json({ secret: "resource" });
        }catch(err){
            next(err);
        }
    }
}