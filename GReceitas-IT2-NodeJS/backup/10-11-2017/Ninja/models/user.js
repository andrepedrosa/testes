const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const Schema = mongoose.Schema;


const userSchema = new Schema({
    nome: String,
    email: {
        type: String,
        required: true,
        //unique: true,
        lowercase: true
    },
    password: {
        type: String,
        minlength: 8,
        required: true
    }, 
    medico: {
        type: Boolean,
        required: true
    },
    utente: {
        type: Boolean,
        default: true
        //required: true 
    },
    farmaceutico: {
        type: Boolean,
        required: true  
    }
});



userSchema.pre('save', async function(next){
    try{
        //generate a salt
        const salt = await bcrypt.genSalt(10);
        //generate a password hash (salt + hash)
        const passwordHash = await bcrypt.hash(this.password,salt);
        //re-assign hashed version over original plain text password
        this.password = passwordHash;
        next();
        /*
        console.log('salt', salt);
        console.log('normal password', this.password);
        console.log('hashed password', passwordHash);
        */
    }catch(err){
        next(err);
    }
});

userSchema.methods.isValidPassword = async function(newPassword){
    try{
        return await bcrypt.compare(newPassword, this.password);
    }catch(err){
        throw new Error(err);
    }
}

const User = mongoose.model('user', userSchema);
module.exports = User;