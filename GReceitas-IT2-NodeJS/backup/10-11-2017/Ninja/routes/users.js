const express = require('express');
const router = express.Router();


const { validateBody, schemas } = require('../helpers/routeHelpers');
const UsersController = require('../controllers/Users');

router.route('/')
    .get(UsersController.index);

router.route('/signup')
    .post(UsersController.signUp);

router.route('/signin')
    .post(UsersController.signIn);

router.route('/secret')
    .get(UsersController.secret);



module.exports = router;