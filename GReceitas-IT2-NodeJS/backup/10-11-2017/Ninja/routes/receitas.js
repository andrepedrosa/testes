const express = require('express');
const router = express.Router();


const ReceitasController = require('../controllers/Receitas');


router.route('/')
.get(ReceitasController.index)
.post(ReceitasController.newReceita);


router.route('/:ReceitaId')
.get(ReceitasController.getReceitaById)
.put(ReceitasController.replaceReceitaById)
.delete(ReceitasController.delReceita);

router.route('/:ReceitaId/Prescricao')
.post(ReceitasController.newPrescricao)
.get(ReceitasController.getPrescricoes);

router.route('/:ReceitaId/Prescricao/:PrescricaoId')
.put(ReceitasController.replacePrescricao)
.delete(ReceitasController.delPrescricao);


router.route('/:ReceitaId/Prescricao/:PrescricaoId/Aviamento')
.post(ReceitasController.newAviamento)
.get(ReceitasController.getAviamentos);

router.route('/Prescricoes/')
.get(ReceitasController.getAllPrescricoes);

router.route('/Aviamentos/:AviamentoId')
.get(ReceitasController.getAviamentoById);

router.route('/Prescricoes/:PrescricaoId')
.get(ReceitasController.getPrescricaoById);

router.route('/Medico/:MedicoId')
.get(ReceitasController.getReceitasMedico);

router.route('/Utente/:UtenteId')
.get(ReceitasController.getReceitasUtente);

router.route('/:ReceitaId/Emitir')
.put(ReceitasController.emitirReceita);


//Utente/{id}/prescricao/poraviar/{?data} 
router.route('/Utente/:UtenteId/Prescricao/Poraviar/:data?')
.get(ReceitasController.prescricoesPorAviar);


module.exports = router;