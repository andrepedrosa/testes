const express = require('express');
const router = express.Router();

const { validateBody, schemas } = require('../helpers/routeHelpers');
const UtilizadoresController = require('../controllers/Utilizadores');


router.route('/')
//.get(UtilizadoresController.index)
.get(UtilizadoresController.index);

router.route('/:UtilizadorId')
.get(UtilizadoresController.getUtilizadorById)
.put(UtilizadoresController.replaceUtilizadorById);

router.route('/signup')
//.post(validateBody(schemas.authSchema), UtilizadoresController.signUp);
.post(UtilizadoresController.signUp);

router.route('/signin')
.post(validateBody(UtilizadoresController.signIn));

router.route('/secret')
.get(UtilizadoresController.secret);


module.exports = router;