const express = require('express');
const router = express.Router();

const MedicosController = require('../controllers/Medicos');


router.route('/')
.get(MedicosController.index)
.post(MedicosController.newMedico);

router.route('/:MedicoId')
.get(MedicosController.getMedicoById)
.put(MedicosController.replaceMedicoById);

//router.route('/:MedicoId/Receitas')
//.get(MedicosController.getMedicoReceitas)



module.exports = router;