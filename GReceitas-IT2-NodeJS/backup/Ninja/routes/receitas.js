const express = require('express');
const router = express.Router();

const ReceitasController = require('../controllers/Receitas');


router.route('/')
.get(ReceitasController.index)
.post(ReceitasController.newReceita);


router.route('/:ReceitaId')
.get(ReceitasController.getReceitaById)
.put(ReceitasController.replaceReceitaById);


module.exports = router;