const express = require('express');
const router = express.Router();
const passport = require('passport');
const passportConf = require('../passport');

const { validateBody, schemas } = require('../helpers/routeHelpers');

const UtentesController = require('../controllers/Utentes');


router.route('/')
.get(passport.authenticate('jwt', { session: false}), UtentesController.index)
//.get(UtentesController.index)
.post(UtentesController.newUtente);

router.route('/:UtenteId')
.get(UtentesController.getUtenteById)
.put(UtentesController.replaceUtenteById);

//router.route('/:UtenteId/Receitas')
//.get(UtentesController.getUtenteReceitas)
//.post(UtentesController.newUtenteReceita);


module.exports = router;


