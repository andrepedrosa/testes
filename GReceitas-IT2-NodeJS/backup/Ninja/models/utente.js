const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const utenteSchema = new Schema({
    firstname: String,
    lastname: String,
    email: String,
    receitas: [{
        type: Schema.Types.ObjectId,
        ref: 'receita'
    }]
});

const Utente = mongoose.model('utente',utenteSchema);
module.exports = Utente;

