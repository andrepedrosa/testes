const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const receitaSchema = new Schema({
    data : { type : Date, default: Date.now },
    utente: {
        type: Schema.Types.ObjectId,
        ref: 'utente'
    },
    medico: {
        type: Schema.Types.ObjectId,
        ref: 'medico'
    },
    prescricao : [{
        apresentacao : String,
        dataValidade : Date,
        quantidade : Number,
        aviamentos : [{
            dataValidade : Date,
            quantidade : Number
        }]
    }]

});

const Receita = mongoose.model('receita',receitaSchema);
module.exports = Receita;