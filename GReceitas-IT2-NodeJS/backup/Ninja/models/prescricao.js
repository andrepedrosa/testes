const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const prescricaoSchema = new Schema({
    data : { type : Date },
    quantidade : { type : Number },
    apresentacao: { type: String },
    receita: {
        type: Schema.Types.ObjectId,
        ref: 'receita'
    },
    
});

const Prescricao = mongoose.model('prescricao',prescricaoSchema);
module.exports = Prescricao;