const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const medicoSchema = new Schema({
    firstname: String,
    lastname: String,
    especialidade: String,
    email: String,
    receitas: [{
        type: Schema.Types.ObjectId,
        ref: 'receita'
    }]
});

const Medico = mongoose.model('medico',medicoSchema);
module.exports = Medico;