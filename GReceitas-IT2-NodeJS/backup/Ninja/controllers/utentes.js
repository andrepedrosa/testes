const Utente = require('../models/utente');
const Receita = require('../models/receita');

module.exports = {

    //get all with Async / Await
    
    index: async function(req, res, next){
        try{
            const utentes = await Utente.find({});
            res.status(200).json(utentes);
        }
        //existe um modulo que se chama express-promise-route que pode ser instalado e que deixa se ser necessário o try catch
        //https://www.youtube.com/watch?v=9tLA4r-gqds
        //minuto 14
        catch(err){
            next(err);
        }  
    },
    

    //post new utente with async / await
    newUtente: async (req,res,next) =>{
        try{
            const newUtente = new Utente(req.body);
            const utente = await newUtente.save();
            res.status(201).json(utente);  
        }
        catch(err){
            next(err);
        }
    },


    //Queries by ID
    // Get by ID using async / await
    getUtenteById: async function(req,res,next){
        try{
            const {utenteId} = req.params;
            //equivalente a:
            //const userId = req.params.userId;
            //console.log("req.params.userId", userId);
            const utente = await Utente.findById(utenteId);
            res.status(200).json(utente);
        }
        catch(err){
            next(err);
        }    
    },
    

    //Put an utente by id
    replaceUtenteById: async function(req,res,next){
        try{
            const {utenteId} = req.params;
            const newUtente = req.body;
            const result = await Utente.findByIdAndUpdate(utenteId,newUtente);
            const utente = await Utente.findById(utenteId);
            res.status(200).json(utente);
        }
        catch(err){
            next(err);
        }
    },

    //Get Utente Receitas
    getUtenteReceitas: async function(req,res,next){
        try{
            const {utenteId} = req.params;
            const utente = await Utente.findById(utenteId).populate('receitas');
            res.status(200).json(utente.receitas);
        }
        catch(err){
            next(err);
        }
    }


  
}
/*
We can interact with mongoose in 3 different ways:
1) Callbacks - worst way
2) Promises
3) Async / Await (Promises) - best way
*/