const Receita = require('../models/receita');
const Medico = require('../models/medico');
const Utente = require('../models/utente');


module.exports = {


    //get all with Async / Await
    
    index: async function(req, res, next){
        try{
            console.log("XXXXXXXXXXXXXXXXXXXX");
            const receitas = await Receita.find({});
            res.status(200).json(receitas);
        }
        catch(err){
            next(err);
        }  
    },


    //post new receita with async / await
    newReceita: async function(req, res, next) {
        try 
        {
            console.log('body', req.body);
            console.log('params', req.params);
            
            const receita = new Receita(req.body);

            await receita.save();
            
            return res.status(201).json(receita);
        }
        catch(err) {
            next(err);
        }
    },


    //Queries by ID
    // Get by ID using async / await
    getReceitaById: async function(req,res,next){
        try{
            const {receitaId} = req.params;
            const receita = await Receita.findById(receitaId);
            res.status(200).json(receita);
        }
        catch(err){
            next(err);
        }    
    },
    

    //Put an receita by id
    replaceReceitaById: async function(req,res,next){
        try{
            const {receitaId} = req.params;
            const newReceita = req.body;
            const result = await Receita.findByIdAndUpdate(receitaId,newReceita);
            const receita = await Receita.findById(receitaId);
            res.status(200).json(receita);
        }
        catch(err){
            next(err);
        }
    }

    
}














