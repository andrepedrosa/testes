const express = require('express');
const router = express.Router();
const passport = require('passport');
const passportConf = require('../passport');

const { validateBody, schemas } = require('../helpers/routeHelpers');
const UtilizadoresController = require('../controllers/Utilizadores');


router.route('/')
//.get(UtilizadoresController.index)
.get(passport.authenticate('jwt', { session: false}), UtilizadoresController.index)

router.route('/:UtilizadorId')
.get(UtilizadoresController.getUtilizadorById)
.put(UtilizadoresController.replaceUtilizadorById);

router.route('/signup')
//.post(validateBody(schemas.authSchema), UtilizadoresController.signUp);
.post(UtilizadoresController.signUp);

router.route('/signin')
.post(validateBody(schemas.authSchema), passport.authenticate('local', { session: false}), UtilizadoresController.signIn);

router.route('/secret')
.get(passport.authenticate('jwt', { session: false}), UtilizadoresController.secret);


module.exports = router;