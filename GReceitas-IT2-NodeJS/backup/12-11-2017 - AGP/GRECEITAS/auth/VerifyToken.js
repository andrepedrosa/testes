const JWT = require('jsonwebtoken');
const { JWT_SECRET } = require('../configuration');

function verifyToken(req,res,next){
    var token = req.headers['authorization'];
    //console.log('x-access-token: ',req.headers['authorization']);
    if(!token)
        return res.status(403).send({
            auth: false, message: 'No token provided.'});

    JWT.verify(token, JWT_SECRET, function(err, decoded){
        console.log("decoded", decoded);
        if(err)
        return res.status(500).send({
            auth: false, message: 'Failed to authenticate token.'
        });

    req.user = decoded.user;
    console.log("decoded.user: ", decoded.user);
    next();
    });    
}

module.exports = verifyToken;