const JWT = require('jsonwebtoken');
const User = require('../models/user');
const { JWT_SECRET } = require('../configuration');
const bcrypt = require('bcrypt');

const index = async (req,res,next) => {
    try{
        const users = await User.find({});
        res.status(200).json(users);
    }catch(err){
        next(err);
    }
};

const signUp = async (req,res,next) => {
    try{
        const nome = req.body["nome"];
        const email = req.body["email"];
        const password = req.body["password"];
        const medico = req.body["medico"];
        const utente = req.body["utente"];
        const farmaceutico = req.body["farmaceutico"];

        
        //check is user already exists
        const foundUser = await User.findOne({ email: email});
        if(foundUser){
            //403 - forbiden
            return res.status(403).json({ error: "Email already in use"});
        }
        

        //create new user
        var newUser = new User();
        newUser.nome = nome;
        newUser.email = email;
        newUser.password = password;
        newUser.medico = medico;
        newUser.utente = utente;
        newUser.farmaceutico = farmaceutico;

        console.log("newuser: ", newUser);
        await newUser.save();

        //generate the token
        //const token = signToken(newUser);

        //respond with token
        res.status(200).json(newUser);
        

    }catch(err){
        next(err);
    }
};

const signIn =  async (req,res,next) => {
    try{
        const password = req.body.password;
        console.log("req.body.password: ",req.body.password);
        console.log("req.body.email: ",req.body.email);
        user = await User.findOne({email: req.body.email});
        console.log("user email:", user.email);
        console.log(user.password);
        console.log(req.user);


        if (!user) {
            res.json({ success: false, message: 'Authentication failed. User not found.' });
        } else if (user) {
    
            // check if password matches

            bcrypt.compare(password, user.password, function(err, resposta) {
                if(resposta) {
                    // Passwords match
                    //console.log("Successful login!")
                    //generate a token

                    const payload = {
                        user: user.email
                    };
                    const token = JWT.sign(payload,JWT_SECRET, {
                        expiresIn: 3600 // 1 hora
                    });
            
                    // return the information including token as JSON
                    res.status(200).json({
                        success: true,
                        message: 'Enjoy your token!',
                        token: token
                    });
                } else {
                    // Passwords don't match
                    res.json({ success: false, message: 'Authentication failed. Wrong password.' });
                } 
              });
            
        }
                 
    }catch(err){
        next(err);
    }
};

//neste caso vai verificar se e medico
const secret = async (req,res,next) => {
    try{
        console.log('req.user', req.user);
        hasRole(req.user,'farmaceutico', async function(decision){
            console.log("decision", decision);
            if(!decision){
                console.log("ENTROUUUU");
                return res.status(403).send(
                    { auth: false, token: null, message: 'You have no authorization.'});
                
            }    
            else{
                User.find(function(err,users){
                    if(err)
                        res.send(err);
                    res.json(users);
                })  
            }   
        });
       
    }catch(err){
        next(err);
    }
};

const hasRole = async (userEmail, role, func) => {
    User.findOne({
        email: userEmail
    }, function(err,user){
        if(err) throw err;

        if(!user){
            res.json({ success: false, message: 'Authentication failed.'});
        } else if(user){
            if(role==='medico'){
                func(role === 'medico' && user.medico === true )
            }else if(role === 'farmaceutico'){
                func(role === 'farmaceutico' && user.farmaceutico === true)
            }     
        }
    })
};


module.exports = {
    index,
    signUp,
    signIn,
    secret,
    hasRole      
};