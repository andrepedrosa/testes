const express = require('express');
const router = express.Router();
const passport = require('passport');
const passportConf = require('../passport');

const { validateBody, schemas } = require('../helpers/routeHelpers');

const UtilizadoresController = require('../controllers/Utilizadores');


router.route('/')
//.get(passport.authenticate('jwt', { session: false}), UtilizadoresController.index)
.get(UtilizadoresController.index)
.post(UtilizadoresController.newUtilizador);

router.route('/:UtilizadorId')
.get(UtilizadoresController.getUtilizadorById)
.put(UtilizadoresController.replaceUtilizadorById);

//router.route('/:UtilizadorId/Receitas')
//.get(UtilizadoresController.getUtilizadorReceitas)
//.post(UtilizadoresController.newUtilizadorReceita);


module.exports = router;