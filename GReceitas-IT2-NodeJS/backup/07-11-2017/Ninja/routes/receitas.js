const express = require('express');
const router = express.Router();

const ReceitasController = require('../controllers/Receitas');


router.route('/')
.get(ReceitasController.index)
.post(ReceitasController.newReceita);


router.route('/:ReceitaId')
.get(ReceitasController.getReceitaById)
.put(ReceitasController.replaceReceitaById);

router.route('/:ReceitaId/Prescricao')
.put(ReceitasController.newPrescricao)
.get(ReceitasController.getPrescricoes);

router.route('/:ReceitaId/Prescricao/:PrescricaoId')
.put(ReceitasController.replacePrescricao);


router.route('/:ReceitaId/Prescricao/:PrescricaoId/Aviamento')
.put(ReceitasController.newAviamento)
.get(ReceitasController.getAviamentos);

router.route('/Prescricoes')
.get(ReceitasController.getAllPrescricoes);

router.route('/Aviamentos/:AviamentoId')
.get(ReceitasController.getAviamentoById);

router.route('/:ReceitaId/Emitir')
.put(ReceitasController.emitirReceita);

module.exports = router;