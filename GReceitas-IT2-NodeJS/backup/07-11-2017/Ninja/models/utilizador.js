const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const utilizadorSchema = new Schema({
    nome: String,
    email: {
        type: String,
        required: true,
        //unique: true,
        lowercase: true
    },
    medico: Boolean,
    utente: Boolean,
    farmaceutico: Boolean,
});

const Utilizador = mongoose.model('utilizador',utilizadorSchema);
module.exports = Utilizador;