const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const receitaSchema = new Schema({
    emitida: { type: Boolean, default: false },
    data : { type : Date, default: Date.now },
    utente: {
        type: Schema.Types.ObjectId,
        ref: 'utilizador',
        required: true
    },
    medico: {
        type: Schema.Types.ObjectId,
        ref: 'utilizador',
        required: true
    },
    prescricoes : [{
        apresentacao : String,
        dataValidade : Date,
        quantidade : Number,
        aviamentos : [{
            dataAviamento : Date,
            quantidade : Number
        }]
    }]

});

const Receita = mongoose.model('receita',receitaSchema);
module.exports = Receita;