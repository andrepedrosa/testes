const Receita = require('../models/receita');
const Utilizador = require('../models/utilizador');
var nodemailer = require('nodemailer');



module.exports = {




    // ******** RECEITAS ********




    //Get all receitas
    //router.route('/')
    //.get(ReceitasController.index)

    index: async function(req, res, next){
        try{
            const receitas = await Receita.find({});
            res.status(200).json(receitas);
        }
        catch(err){
            next(err);
        }  
    },


    //Get receita por ID
    //router.route('/:ReceitaId')
    //.get(ReceitasController.getReceitaById);

    getReceitaById: async function(req,res,next){
        try{
            const {receitaId} = req.params;
            const receita = await Receita.findById(receitaId);
            res.status(200).json(receita);
        }
        catch(err){
            next(err);
        }    
    },



    //Post receita
    //router.route('/')
    //.post(ReceitasController.newReceita);

    newReceita: async function(req, res, next) {
        try 
        {
            console.log('body', req.body["utente"]);
            console.log('params', req.params);

            //Validacoes

            //receber do body o id do utente
            const utenteId = req.body["utente"];
            //receber do body o id do medico
            const medicoId = req.body["medico"];
            //valida se sao o mesmo utilizador
            if(utenteId==medicoId){
                return res.status(403).json({ error: "O medico e utente nao podem ter o mesmo id"});
            }
            //procurar em utilizadores se existe o utente que é passado no body 
            const utente = await Utilizador.findById(utenteId);
            console.log('procura utente', utente);
            if(!utente){
                return res.status(403).json({ error: "O Utente nao existe"});
            }else{
                if(!utente["utente"]){
                    return res.status(403).json({ error: "O Utente nao existe"});
                }
            }
            //procurar em utilizadores se existe o medico que é passado no body 
            const medico = await Utilizador.findById(medicoId);
            console.log('procura medico', medico);
            if(!medico){
                return res.status(403).json({ error: "O Medico nao existe"});
            }else{
                if(!medico["medico"]){
                    return res.status(403).json({ error: "O Medico nao existe"});
                }
            }

            
            //grava receita se utente e medico existirem no documento utilizadores
            const receita = new Receita(req.body);

            await receita.save();
            
            return res.status(201).json(receita);
            
        }
        catch(err) {
            next(err);
        }
    },


    //Modifica receita por id
    //router.route('/:ReceitaId')
    //.put(ReceitasController.replaceReceitaById);

    replaceReceitaById: async function(req,res,next){
        try{
            const {receitaId} = req.params;
            const newReceita = req.body;
            const result = await Receita.findByIdAndUpdate(receitaId,newReceita);
            const receita = await Receita.findById(receitaId);
            res.status(200).json(receita);
        }
        catch(err){
            next(err);
        }
    },





    // ******** PRESCRICOES ********





    //Get todas Prespricoes
    //router.route('/Prescricoes')
    //.get(ReceitasController.getAllPrescricoes);

    getAllPrescricoes: async function(req, res, next) {
        try{

            //Porque eh que ele nao entra nesta route
            //Como funcionam os nomes das routes?


            console.log("XXXXX XXXX")
            const receitas = await Receita.find({});
            var allPrescricoes;
  
            for(var i in receitas) {
                const prescricoes=receitas[i]["prescricoes"];
                for (var j = 0; j < prescricoes.length; j++) {
                    allPrescricoes.push(prescricoes[j]);
                    console.log(allPrescricoes);
                }           
            }

            //res.status(200).json(allPrescricoes);

        }catch(err){
            next(err);
        }
    },



    //Get Prespricoes de uma receita
    //router.route('/:ReceitaId/Prescricao')
    //.get(ReceitasController.getPrescricoes);

    getPrescricoes: async function(req, res, next) {
        try{
            //obter o id da receita nos parametros
            const {receitaId} = req.params;
            const newreceita = await Receita.findOne(receitaId);
            //console.log(newreceita["prescricoes"]);
            res.status(200).json(newreceita["prescricoes"]); 
        }
        catch(err){
            next(err);
        }
    },



    //Adicionar prescrições por receita
    //router.route('/:ReceitaId/Prescricao')
    //.put(ReceitasController.newPrescricao);

    newPrescricao: async function(req, res, next) {
        try{
            //obter o id da receita nos parametros
            const {receitaId} = req.params;
            //obter a prescricao que esta no body
            const prescricaoBody = req.body;
            //procurar a receita na bd pelo id
            const newreceita = await Receita.findOne(receitaId);
            newreceita.prescricoes.push(prescricaoBody);
            const result = await newreceita.save();  
            res.status(200).json(newreceita);  
        }
        catch(err){
            next(err);
        } 
    },



    //Modificar uma prescricao por receita
    //router.route('/:ReceitaId/Prescricao/:PrescricaoId')
    //.put(ReceitasController.replacePrescricao);

    replacePrescricao: async function(req, res, next) {
        try{
            
            //obter o id da receita nos parametros
            const receitaId = req.params["ReceitaId"];
            //procurar a receita na bd pelo id
            const newReceita = await Receita.findById(receitaId);
            //Obter a lista de prescricoes da receita
            const prescricoes = newReceita["prescricoes"];
            //testar se já houve aviamento (ver se lista de aviamentos vazia)
            const aviamentos = prescricoes.aviamentos;
            
            if(!(aviamentos && aviamentos.length)){
                const prescricaoId = req.params["PrescricaoId"];
                //obter a prescricao que esta no body
                const prescricaoBody = req.body;
    
                index = await prescricoes.findIndex(elem => elem._id==prescricaoId);
                prescricoes.splice(index,1);     
                prescricoes.push(prescricaoBody);
                const result = await newReceita.save();
                res.status(200).json(prescricoes);
            }

        }
        catch(err){
            next(err);
        } 
    },



    

    // ******** AVIAMENTOS ********




    //POST de um Aviamento
    //router.route('/:ReceitaId/Prescricao/:PrescricaoId/Aviamento')
    //.put(ReceitasController.newAviamento)
    
    newAviamento: async function(req, res, next) {
        try{
            //quantidade na prescricao passada no body
            const quantidadeBody = req.body.quantidade;
            
            
            const receitaId = req.params["ReceitaId"];
            const prescricaoId = req.params["PrescricaoId"];
            const newReceita = await Receita.findById(receitaId);
            const prescricoes = newReceita["prescricoes"];
            prescricao = await prescricoes.find(elem => elem._id==prescricaoId);
            
            quantidadePrescricao = prescricao.quantidade;
            console.log(quantidadePrescricao);

            if(quantidadePrescricao>=quantidadeBody){
                prescricao.quantidade-=quantidadeBody;
            
                const aviamento = req.body;
                aviamentos = prescricao.aviamentos;
                aviamentos.push(aviamento);
    
                const result = await newReceita.save();
                res.status(200).json({prescricao}); 
            }else{
                res.status(200).json("Erro - A quantidade da prescricao eh inferior a quantidade que deseja aviar");
                
            }
        }
        catch(err){
            next(err);
        }
    },



    //Get aviamentos de uma prescricao
    //router.route('/:ReceitaId/Prescricao/:PrescricaoId/Aviamento')
    //.get(ReceitasController.getAviamentos);

    getAviamentos: async function(req,res,next){
        try{
            const receitaId = req.params["ReceitaId"];
            const prescricaoId = req.params["PrescricaoId"];
            const newReceita = await Receita.findById(receitaId);
            const prescricoes = newReceita["prescricoes"];
            prescricao = await prescricoes.find(elem => elem._id==prescricaoId);
            aviamentos = prescricao.aviamentos;
            res.status(200).json(aviamentos);

        }catch(err){
            next(err);
        }
    },

    

    //Get aviamento por id
    //router.route('/Aviamentos/:AviamentoId')
    //.get(ReceitasController.getAviamentoById);

    getAviamentoById: async function(req,res,next){
        try{
            //obter todas as receitas
            //pesquisar em cada recita a lista das prescicoes e verificar se alguma delas tem o id igual ao passado no parametro
            //escrever

            //todas as receitas
            const receitas = await Receita.find({});
            const aviamentoId = req.params["AviamentoId"];
  
            for(var i in receitas) {
                const prescricoes=receitas[i]["prescricoes"];
                for (var j = 0; j < prescricoes.length; j++) {
                    const aviamentos=prescricoes[j].aviamentos;
                    for (var z = 0; z < aviamentos.length; z++){
                        if(aviamentoId==aviamentos[z]._id){
                            return res.status(200).json(aviamentos[z]);
                        }
                    }
                }           
            }

            return res.status(404).json("Aviamento id nao encontrado");

        }catch(err){
            next(err);
        }
    },
    






    //Emitir receita ... enviar email ao utente
    //router.route('/:ReceitaId/Emitir')
    //.put(ReceitasController.emitirReceita);

    emitirReceita: async function(req, res, next) {
        try{
            //obter o id da receita nos parametros
            const {receitaId} = req.params;
            const newReceita = await Receita.findOne(receitaId);

            const utilizador = await Utilizador.findById(newReceita["utente"]);
            //console.log(newReceita["utente"]);

            var enviado=false;

            const utenteEmail = Utilizador["email"];

            
            if(newReceita["emitida"]===false){
                newReceita["emitida"]=true;
                

                const utilizador = await Utilizador.findOne(newReceita["utente"]);
                const utenteEmail = Utilizador["email"];

                var transporter = nodemailer.createTransport({
                    service: 'Hotmail',
                    auth: {
                        user: 'gestor.receitas@hotmail.com', // Your email id
                        pass: 'GESTOR?receitas' // Your password
                    }
                });
    
                var text = 'Receita emitida: \n\n' + newReceita;           

                var mailOptions = {
                    from: '<gestor.receitas@hotmail.com>', // sender address
                    to: 'utente.receitas@hotmail.com', // list of receivers
                    subject: 'Emissao de Receita', // Subject line
                    text: text //, // plaintext body
                };

   
                transporter.sendMail(mailOptions, function(error, info){
                    if(error){
                        console.log(error);
                        res.json({yo: 'error'});
                    }else{
                        enviado=true;
                        console.log('Message sent: ' + info.response);
                        res.json({yo: info.response});
                    };
                });
                
                
            }

            if(enviado==true){
                const result = await newReceita.save();
                res.status(200).json(newReceita); 
            }
            
        }
        catch(err){
            next(err);
        }
    }


 

    
}














