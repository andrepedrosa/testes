const JWT = require('jsonwebtoken');
const User = require('../models/user');
const { JWT_SECRET } = require('../configuration');

signToken = function(user){
    return JWT.sign({
        iss: 'GReceitas',
        sub: user.id,
        iat: new Date().getTime(),
        //exp: new Date().setDate(new Date().getTime() + 2*60000)
        //(new Date().getTime() + 1*60000)
        //data de expiracao + 1 dia apos data do sign
        exp: new Date().setDate(new Date().getDate()+1)
    }, JWT_SECRET);
}

module.exports = {
    index: async function(req,res,next){
        try{
            const users = await User.find({});
            res.status(200).json(users);
        }catch(err){
            next(err);
        }
    },
    signUp: async function(req,res,next){
        try{
            const email = req.value.body.email;
            const password = req.value.body.password;

            //check is user already exists
            const foundUser = await User.findOne({ email: email});
            if(foundUser){
                //403 - forbiden
                return res.status(403).json({ error: "Email already in use"});
            }

            //create new user
            const newUser = new User({
                email: email,
                password: password
            });
            await newUser.save();

            //generate the token
            const token = signToken(newUser);

            //respond with token
            res.status(200).json({ token: token});

        }catch(err){
            next(err);
        }
    },

    signIn: async function(req,res,next){
        try{
            //console.log("Successful login!")
            //generate a token

            const token = signToken(req.user);
            res.status(200).json({token: token});
        }catch(err){
            next(err);
        }
    },

    secret: async function(req,res,next){
        try{
            console.log('I managed to get here!');
            res.json({ secret: "resource" });
        }catch(err){
            next(err);
        }
    }
}