const Utilizador = require('../models/utilizador');
const Receita = require('../models/receita');

module.exports = {

    //get all with Async / Await
    
    index: async function(req, res, next){
        try{
            const utilizadores = await Utilizador.find({});
            res.status(200).json(utilizadores);
        }
        //existe um modulo que se chama express-promise-route que pode ser instalado e que deixa se ser necessário o try catch
        //https://www.youtube.com/watch?v=9tLA4r-gqds
        //minuto 14
        catch(err){
            next(err);
        }  
    },
    

    //post new utilizador with async / await
    newUtilizador: async (req,res,next) =>{
        try{
            const newUtilizador = new Utilizador(req.body);
            const utilizador = await newUtilizador.save();
            res.status(201).json(utilizador);  
        }
        catch(err){
            next(err);
        }
    },


    //Queries by ID
    // Get by ID using async / await
    getUtilizadorById: async function(req,res,next){
        try{
            const {utilizadorId} = req.params;
            //equivalente a:
            //const utilizadorId = req.params.utilizadorId;
            //console.log("req.params.utilizadorId", utilizadorId);
            const utilizador = await Utilizador.findById(utilizadorId);
            res.status(200).json(utilizador);
        }
        catch(err){
            next(err);
        }    
    },
    

    //Put an utilizador by id
    replaceUtilizadorById: async function(req,res,next){
        try{
            const {utilizadorId} = req.params;
            const newUtilizador = req.body;
            const result = await Utilizador.findByIdAndUpdate(utilizadorId,newUtilizador);
            const utilizador = await Utilizador.findById(utilizadorId);
            res.status(200).json(utilizador);
        }
        catch(err){
            next(err);
        }
    },

    //Get Utilizador Receitas
    getUtilizadorReceitas: async function(req,res,next){
        try{
            const {utilizadorId} = req.params;
            const utilizador = await Utilizador.findById(utilizadorId).populate('receitas');
            res.status(200).json(utilizador.receitas);
        }
        catch(err){
            next(err);
        }
    }


  
}
/*
We can interact with mongoose in 3 different ways:
1) Callbacks - worst way
2) Promises
3) Async / Await (Promises) - best way
*/