const express = require('express');
const logger = require('morgan');
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const http = require('http');
const cors = require('cors');

const port = 3000;


mongoose.Promise = global.Promise;
//mongoose.connect('mongodb://localhost/apiprojecttest', { useMongoClient: true });
var uri = 'mongodb://1071205:greceitas2017@ds237475.mlab.com:37475/greceitas';
db = mongoose.connect(uri);
mongoose.set('debug', true);

const app =  express();

//Routes
const receitas = require('./routes/receitas')
const users = require('./routes/users')
const apresentacoes = require('./routes/apresentacoes')
const medicamentos = require('./routes/medicamentos')
const farmacos = require('./routes/farmacos')
const pacientes = require('./routes/pacientes')
const medicos = require('./routes/medicos')
const farmaceuticos = require('./routes/farmaceuticos')



//Middlewares
app.use(logger('combined'));
app.use(bodyParser.json());
app.use(cors());

//Routes
app.use('/users', users);
app.use('/receitas', receitas);
app.use('/apresentacoes', apresentacoes);
app.use('/medicamentos', medicamentos);
app.use('/farmacos', farmacos);
app.use('/pacientes', pacientes);
app.use('/medicos', medicos);
app.use('/farmaceuticos', farmaceuticos);


//Catch 404 errors and foward them to error handler
app.use(function(req,res,next){
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

//Error handler function
app.use(function(err,req,res,next){
    const error = app.get('env') === 'development' ? err : {};
    const status = err.status || 500;
    //Respond to client
    res.status(status).json({
        error:{
            message: error.message
        }
    });
    //Respond to ourselves
    console.error(err);
});


//Start the server
app.listen(port, (err) => {
    console.log(`running server on port: ${port}`);
});