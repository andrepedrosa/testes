const Medicamento = require('../models/medicamento');
const User = require('../models/user');
var nodemailer = require('nodemailer');
const UsersController = require('../controllers/Users');


module.exports = {

    //Get all medicamentos
    //router.route('/')
    //.get(MedicamentosController.getMedicamentos);
    getMedicamentos: async function (req, res, next) {
        try {
            console.log("router.route('/')");
            console.log(".get(MedicamentosController.getMedicamentos)");
            console.log("req.path", req.path);
           
            const medicamentos = await Medicamento.find({});

            res.status(200).json(medicamentos);
        }
        catch (err) {
            next(err);
        }
    },

    //Get medicamento por id
    //router.route('/:MedicamentoId')
    //.get(MedicamentosController.getMedicamentoById);
    getMedicamentoById: async function (req, res, next) {
        try {
            console.log("router.route('/:MedicamentoId')");
            console.log(".get(MedicamentosController.getMedicamentoById)");
            console.log("req.path", req.path);


            const medicamentoId = req.params["MedicamentoId"];
            const medicamento = await Medicamento.findById(medicamentoId);
            res.status(200).json(medicamento);
        }
        catch (err) {
            next(err);
        }
    },


    //post new medicamento
    //router.route('/')
    //.post(MedicamentosController.newMedicamento)
    newMedicamento: async function (req, res, next) {
        try {
            console.log("router.route('/')");
            console.log(".post(MedicamentosController.newMedicamento)")


            const medicamento = new Medicamento(req.body);

            await medicamento.save();

            return res.status(201).json(medicamento);

        }
        catch (err) {
            next(err);
        }
    },

}    