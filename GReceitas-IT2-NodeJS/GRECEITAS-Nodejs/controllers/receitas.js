const Receita = require('../models/receita');
const User = require('../models/user');
var nodemailer = require('nodemailer');
const UsersController = require('../controllers/Users');


module.exports = {


    // ******** RECEITAS ********




    //Get all receitas
    //router.route('/')
    //.get(ReceitasController.index)

    index: async function (req, res, next) {
        try {
            console.log("router.route('/')");
            console.log(".get(ReceitasController.index)");

            UsersController.hasRole(req.user, 'medico', async function (decision) {
                if (!decision) {
                    return res.status(403).send(
                        { auth: false, token: null, message: 'You have no authorization.' });
                }
                else {
                    const receitas = await Receita.find({});
                    res.status(200).json(receitas);
                }

            })
        }
        catch (err) {
            next(err);
        }
    },



    //Get receita por ID
    //router.route('/:ReceitaId')
    //.get(ReceitasController.getReceitaById);

    getReceitaById: async function (req, res, next) {
        try {
            console.log("router.route('/:ReceitaId')");
            console.log(".get(ReceitasController.index)");
            console.log("req.path", req.path);


            const receitaId = req.params["ReceitaId"];
            const receita = await Receita.findById(receitaId);
            res.status(200).json(receita);
        }
        catch (err) {
            next(err);
        }
    },



    //Post receita
    //router.route('/')
    //.post(ReceitasController.newReceita);

    newReceita: async function (req, res, next) {
        try {
            console.log("router.route('/')");
            console.log(".post(ReceitasController.newReceita)")
            console.log('body', req.body["utente"]);
            console.log('params', req.params);

            //Validacoes

            //receber do body o id do utente
            const utenteId = req.body["utente"];
            //receber do body o id do medico
            const medicoId = req.body["medico"];
            //valida se sao o mesmo user
            if (utenteId == medicoId) {
                return res.status(403).json({ error: "O medico e utente nao podem ter o mesmo id" });
            }
            //procurar em user se existe o utente que é passado no body 
            const utente = await User.findById(utenteId);
            console.log('procura utente', utente);
            if (!utente) {
                return res.status(403).json({ error: "O Utente nao existe" });
            } else {
                if (!utente["utente"]) {
                    return res.status(403).json({ error: "O Utente nao existe" });
                }
            }
            //procurar em users se existe o medico que é passado no body 
            const medico = await User.findById(medicoId);
            console.log('procura medico', medico);
            if (!medico) {
                return res.status(403).json({ error: "O Medico nao existe" });
            } else {
                if (!medico["medico"]) {
                    return res.status(403).json({ error: "O Medico nao existe" });
                }
            }


            //grava receita se utente e medico existirem no documento users
            const receita = new Receita(req.body);

            await receita.save();

            return res.status(201).json(receita);

        }
        catch (err) {
            next(err);
        }
    },


    //Modifica receita por id
    //router.route('/:ReceitaId')
    //.put(ReceitasController.replaceReceitaById);

    replaceReceitaById: async function (req, res, next) {
        try {

            console.log("router.route('/:ReceitaId')");
            console.log(".put(ReceitasController.replaceReceitaById)");
            const receitaId = req.params["ReceitaId"];
            const newReceita = req.body;
            const result = await Receita.findByIdAndUpdate(receitaId, newReceita);
            const receita = await Receita.findById(receitaId);
            res.status(200).json(receita);
        }
        catch (err) {
            next(err);
        }
    },





    // ******** PRESCRICOES ********





    //Get todas Prespricoes
    //router.route('/Prescricoes')
    //.get(ReceitasController.getAllPrescricoes);

    getAllPrescricoes: async function (req, res, next) {
        try {


            console.log("router.route('/Prescricoes')");
            console.log(".get(ReceitasController.getAllPrescricoes)");

            /*
            if(req.path !== "/Prescricoes"){
                next();
            }
            */

            const receitas = await Receita.find({});
            var allPrescricoes = new Array();

            for (var i in receitas) {
                const prescricoes = receitas[i]["prescricoes"];
                for (var j = 0; j < prescricoes.length; j++) {
                    allPrescricoes.push(prescricoes[j]);

                }
            }

            console.log(allPrescricoes);

            //res.status(200).json(allPrescricoes);

        } catch (err) {
            next(err);
        }
    },



    //Get Prespricao por id
    //router.route('/Prescricoes/:PrescricaoId')
    //.get(ReceitasController.getPrescricaoById);

    getPrescricaoById: async function (req, res, next) {
        try {


            console.log("router.route('/Prescricoes/:PrescricaoId')");
            console.log(".get(ReceitasController.getPrescricaoById)");

            const receitas = await Receita.find({});
            const prescricaoId = req.params["PrescricaoId"];

            var prescricao;

            for (var i in receitas) {
                const prescricoes = receitas[i]["prescricoes"];
                for (var j = 0; j < prescricoes.length; j++) {
                    if (prescricoes[j]._id == prescricaoId) {
                        return res.status(200).json(prescricoes[j]);
                    }
                }
            }

            return res.status(404).json("Não encontrou a prescricao com esse id");

            //res.status(200).json(allPrescricoes);

        } catch (err) {
            next(err);
        }
    },



    //Get Prespricoes de uma receita
    //router.route('/:ReceitaId/Prescricao')
    //.get(ReceitasController.getPrescricoes);

    getPrescricoes: async function (req, res, next) {
        try {
            console.log("router.route('/:ReceitaId/Prescricao')");
            console.log(".get(ReceitasController.getPrescricoes)");
            //obter o id da receita nos parametros

            const receitaId = req.params["ReceitaId"];
            console.log(receitaId);

            const newreceita = await Receita.findById(receitaId);
            console.log(newreceita);

            console.log(newreceita["prescricoes"]);

            res.status(200).json(newreceita["prescricoes"]);


        }
        catch (err) {
            next(err);
        }
    },



    //Adicionar prescrições por receita
    //router.route('/:ReceitaId/Prescricao')
    //.post(ReceitasController.newPrescricao);

    newPrescricao: async function (req, res, next) {
        try {
            console.log("router.route('/:ReceitaId/Prescricao')");
            console.log(".post(ReceitasController.newPrescricao)");
            //obter o id da receita nos parametros
            const receitaId = req.params["ReceitaId"];
            //obter a prescricao que esta no body
            const prescricaoBody = req.body;
            //procurar a receita na bd pelo id
            console.log(receitaId);
            const newreceita = await Receita.findById(receitaId);
            console.log(newreceita);
            newreceita.prescricoes.push(prescricaoBody);
            const result = await newreceita.save();
            res.status(200).json(newreceita);
        }
        catch (err) {
            next(err);
        }
    },



    //Modificar uma prescricao por receita
    //router.route('/:ReceitaId/Prescricao/:PrescricaoId')
    //.put(ReceitasController.replacePrescricao);

    replacePrescricao: async function (req, res, next) {
        try {
            console.log("router.route('/:ReceitaId/Prescricao/:PrescricaoId')");
            console.log(".put(ReceitasController.replacePrescricao)");

            const receitaId = req.params["ReceitaId"];
            const prescricaoId = req.params["PrescricaoId"];
            const newReceita = await Receita.findById(receitaId);
            const prescricoes = newReceita["prescricoes"];
            prescricao = await prescricoes.find(elem => elem._id == prescricaoId);
            const aviamentos = prescricao.aviamentos;

            prescricao.apresentacao = req.body.apresentacao;
            prescricao.dataValidade = req.body.dataValidade;
            prescricao.quantidade = req.body.quantidade;

            const result = await newReceita.save();
            res.status(200).json(result);

        }
        catch (err) {
            next(err);
        }
    },








    //Delete uma prescricao por receita
    //router.route('/:ReceitaId/Prescricao/:PrescricaoId')
    //.delete(ReceitasController.delPrescricao);

    delPrescricao: async function (req, res, next) {
        try {
            console.log("router.route('/:ReceitaId/Prescricao/:PrescricaoId')");
            console.log(".delete(ReceitasController.delPrescricao)");
            //obter o id da receita nos parametros
            const receitaId = req.params["ReceitaId"];
            //procurar a receita na bd pelo id
            const newReceita = await Receita.findById(receitaId);
            //Obter a lista de prescricoes da receita
            const prescricoes = newReceita["prescricoes"];
            const prescricaoId = req.params["PrescricaoId"];

            index = await prescricoes.findIndex(elem => elem._id == prescricaoId);
            prescricoes.splice(index, 1);
            const result = await newReceita.save();
            res.status(200).json(prescricoes);


        }
        catch (err) {
            next(err);
        }
    },



    //GET Prescrições por aviar até {data} ou todas (sem data opcional)
    //Utente/{id}/prescricao/poraviar/{?data} 
    //router.route('/Utente/:UtenteId/Prescricao/Poraviar/:data?')
    //.get(ReceitasController.prescricoesPorAviar);
    prescricoesPorAviar: async function (req, res, next) {
        try {
            console.log("router.route('/Utente/:UtenteId/Prescricao/Poraviar/{?data}')");
            console.log(".get(ReceitasController.prescricoesPorAviar)");

            const dataReceita = new Date(req.params["data"]);
            const utenteId = req.params["UtenteId"];
            const receitas = await Receita.find({});
            var prescricoesUtente = new Array();

            if (dataReceita instanceof Date && isFinite(dataReceita)) {

                for (var i in receitas) {
                    const prescricoes = receitas[i]["prescricoes"];
                    if ((utenteId == receitas[i]["utente"]) && (receitas[i]["data"] < dataReceita)) {
                        for (var j = 0; j < prescricoes.length; j++) {
                            prescricoesUtente.push(prescricoes[j]);
                        }
                    }
                }

            } else {
                for (var i in receitas) {
                    const prescricoes = receitas[i]["prescricoes"];
                    if (utenteId == receitas[i]["utente"]) {
                        for (var j = 0; j < prescricoes.length; j++) {
                            prescricoesUtente.push(prescricoes[j]);
                        }
                    }
                }
            }

            //console.log(prescricoesUtente);
            res.status(200).json(prescricoesUtente);


        } catch (err) {
            next(err);
        }
    },



    //router.route('/:ReceitaId')
    //.delete(ReceitasController.delReceita)
    delReceita: async function (req, res, next) {
        try {
            console.log("router.route('/:ReceitaId')");
            console.log(".delete(ReceitasController.delReceita)");
            //obter o id da receita nos parametros
            const receitaId = req.params["ReceitaId"];
            console.log("receitaId", receitaId);
            //procurar a receita na bd pelo id
            const newReceita = await Receita.findByIdAndRemove(receitaId);
            const receitas = await Receita.find();
            res.status(200).json(receitas);

        } catch (err) {

        }
    },


    // ******** AVIAMENTOS ********




    //POST de um Aviamento
    //router.route('/:ReceitaId/Prescricao/:PrescricaoId/Aviamento')
    //.post(ReceitasController.newAviamento)

    newAviamento: async function (req, res, next) {
        try {
            console.log("router.route('/:ReceitaId/Prescricao/:PrescricaoId/Aviamento')");
            console.log(".post(ReceitasController.newAviamento)");

            UsersController.hasRole(req.user, 'farmaceutico', async function (decision) {
                if (!decision) {
                    return res.status(403).send(
                        { auth: false, token: null, message: 'You have no authorization.' });
                }
                else {
                    const quantidadeBody = req.body.quantidade;
                    const receitaId = req.params["ReceitaId"];
                    const prescricaoId = req.params["PrescricaoId"];
                    const newReceita = await Receita.findById(receitaId);
                    const prescricoes = newReceita["prescricoes"];
                    prescricao = await prescricoes.find(elem => elem._id == prescricaoId);
                    const userId = req.body["farmaceutico"];
                    const user = await User.findById(userId);
                    console.log("user", user);
                    console.log("user['farmaceutico']:", user['farmaceutico']);
                    const aviamento = req.body;
                    dataValidadePrespricao = prescricao["dataValidade"];
                    var dataAviamento = new Date(Date.now());

                    console.log("dataValidadePrespricao", dataValidadePrespricao);
                    console.log("dataAviamento", dataAviamento);


                    quantidadePrescricao = prescricao.quantidade;
                    //console.log(quantidadePrescricao);
                    if (user['farmaceutico'] == true) {

                        if (dataValidadePrespricao >= dataAviamento) {
                            if (quantidadePrescricao >= quantidadeBody && quantidadePrescricao > 0) {
                                prescricao.quantidade -= quantidadeBody;

                                aviamentos = prescricao.aviamentos;
                                aviamentos.push(aviamento);

                                const result = await newReceita.save();
                                res.status(200).json({ prescricao });
                            } else {
                                res.status(400).json("Erro - A quantidade da prescricao eh inferior a quantidade que deseja aviar");
                            }
                        } else {
                            res.status(400).json("Erro - Data nao valida: superior a data da prescricao");
                        }

                    } else {
                        res.status(400).json("Erro - User invalido - Precisa de ser farmaceutico");
                    }
                }
            });

        }
        catch (err) {
            next(err);
        }
    },


    //Get aviamentos de uma prescricao
    //router.route('/:ReceitaId/Prescricao/:PrescricaoId/Aviamento')
    //.get(ReceitasController.getAviamentos);

    getAviamentos: async function (req, res, next) {
        try {
            console.log("router.route('/:ReceitaId/Prescricao/:PrescricaoId/Aviamento')");
            console.log(".get(ReceitasController.getAviamentos)");
            const receitaId = req.params["ReceitaId"];
            const prescricaoId = req.params["PrescricaoId"];
            const newReceita = await Receita.findById(receitaId);
            const prescricoes = newReceita["prescricoes"];
            prescricao = await prescricoes.find(elem => elem._id == prescricaoId);
            aviamentos = prescricao.aviamentos;
            res.status(200).json(aviamentos);

        } catch (err) {
            next(err);
        }
    },



    //Get aviamento por id
    //router.route('/Aviamentos/:AviamentoId')
    //.get(ReceitasController.getAviamentoById);

    getAviamentoById: async function (req, res, next) {
        try {
            console.log("router.route('/Aviamentos/:AviamentoId')");
            console.log(".get(ReceitasController.getAviamentoById)");
            //obter todas as receitas
            //pesquisar em cada recita a lista das prescicoes e verificar se alguma delas tem o id igual ao passado no parametro
            //escrever

            //todas as receitas
            const receitas = await Receita.find({});
            const aviamentoId = req.params["AviamentoId"];

            for (var i in receitas) {
                const prescricoes = receitas[i]["prescricoes"];
                for (var j = 0; j < prescricoes.length; j++) {
                    const aviamentos = prescricoes[j].aviamentos;
                    for (var z = 0; z < aviamentos.length; z++) {
                        if (aviamentoId == aviamentos[z]._id) {
                            return res.status(200).json(aviamentos[z]);
                        }
                    }
                }
            }

            return res.status(404).json("Aviamento id nao encontrado");

        } catch (err) {
            next(err);
        }
    },



    // ******** Outros ********



    // //Get receitas por medico
    // //router.route('/Medico/:MedicoId')
    // //.get(ReceitasController.getReceitasMedico);
    // getReceitasMedico: async function (req, res, next) {
    //     try {
    //         /*
    //         if(req.path !== "/Prescricoes"){
    //             next();
    //         }*/

    //         console.log("router.route('/Medico/:MedicoId')");
    //         console.log(".get(ReceitasController.getReceitasMedico)");

    //         const medicoId = req.params["MedicoId"];
    //         const receitas = await Receita.find({});
    //         var receitasMedico = new Array();

    //         for (var i in receitas) {
    //             console.log("receitas[i]['medico']._id", receitas[i]["medico"]);
    //             if (medicoId == receitas[i]["medico"]) {
    //                 receitasMedico.push(receitas[i])
    //             }
    //         }

    //         console.log(receitasMedico);
    //         return res.status(200).json(receitasMedico);


    //     } catch (err) {

    //     }
    // },



    // //Get receitas por medico
    // //router.route('/Medico/:MedicoId')
    // //.get(ReceitasController.getReceitasMedico);
    // getReceitasMedico: async function (req, res, next) {
    //     try {
    //         /*
    //         if(req.path !== "/Prescricoes"){
    //             next();
    //         }*/

    //         console.log("router.route('/Medico/:MedicoId')");
    //         console.log(".get(ReceitasController.getReceitasMedico)");

    //         const medicoId = req.params["MedicoId"];
    //         const receitas = await Receita.find({});
    //         var receitasMedico = new Array();

    //         for (var i in receitas) {

    //             if (medicoId == receitas[i]["medico"]) {
    //                 receitasMedico.push(receitas[i])
    //             }
    //         }

    //         res.status(200).json(receitasMedico);


    //     } catch (err) {

    //     }
    // },



    // //Get receitas por utente
    // //router.route('/Utente/:UtenteId')
    // //.get(ReceitasController.getReceitasUtente);
    // getReceitasUtente: async function (req, res, next) {
    //     try {
    //         /*
    //         if(req.path !== "/Prescricoes"){
    //             next();
    //         }*/

    //         console.log("router.route('/Utente/:UtenteId')");
    //         console.log(".get(ReceitasController.getReceitasUtente)");

    //         const utenteId = req.params["UtenteId"];
    //         const receitas = await Receita.find({});
    //         var receitasUtente = new Array();

    //         for (var i in receitas) {

    //             if (utenteId == receitas[i]["utente"]) {
    //                 receitasUtente.push(receitas[i])
    //             }
    //         }

    //         res.status(200).json(receitasUtente);

    //     } catch (err) {

    //     }
    // },




    //Emitir receita ... enviar email ao utente
    //router.route('/:ReceitaId/Emitir')
    //.put(ReceitasController.emitirReceita);

    emitirReceita: async function (req, res, next) {
        try {
            console.log("router.route('/:ReceitaId/Emitir')");
            console.log(".put(ReceitasController.emitirReceita)");

            UsersController.hasRole(req.user, 'medico', async function (decision) {

                if (!decision) {
                    return res.status(403).send(
                        { auth: false, token: null, message: 'You have no authorization.' });
                } else {
                    //obter o id da receita nos parametros
                    const receitaId = req.params["ReceitaId"];
                    console.log("receitaId", receitaId);
                    const newReceita = await Receita.findById(receitaId);
                    console.log("newReceita", newReceita);
                    const user = await User.findById(newReceita["utente"]);
                    console.log(user);

                    var enviado = false;

                    const utenteEmail = user["email"];


                    if (newReceita["emitida"] === false) {
                        newReceita["emitida"] = true;


                        const user = await User.findOne(newReceita["utente"]);


                        var transporter = nodemailer.createTransport({
                            service: 'Hotmail',
                            auth: {
                                user: 'gestor.receitas@hotmail.com', // Your email id
                                pass: 'GESTOR?receitas' // Your password
                            }
                        });

                        var text = 'Receita emitida: \n\n' + newReceita;

                        var mailOptions = {
                            from: '<gestor.receitas@hotmail.com>', // sender address
                            to: utenteEmail, // list of receivers
                            subject: 'Emissao de Receita', // Subject line
                            text: text //, // plaintext body
                        };


                        transporter.sendMail(mailOptions, function (error, info) {
                            if (error) {
                                console.log(error);
                                res.json({ error });
                            } else {
                                enviado = true;
                                console.log('Message sent: ' + info.response);
                                res.json({ yo: info.response });
                            };
                            transporter.close();
                        });

                    }

                    if (enviado == true) {
                        const result = await newReceita.save();
                        res.status(200).json(newReceita);
                    }
                }

            });
        }
        catch (err) {
            next(err);
        }
    },

    //Receitas Por Paciente
    //router.route('/Receitas/:PacienteId')
    //.get(ReceitasController.getReceitasPorPaciente)
    getReceitasPorPaciente: async function (req, res, next) {
        try {
            console.log("router.route('/Receitas/:PacienteId')");
            console.log(".get(ReceitasController.getReceitasPorPaciente)");

            console.log(req.user);

            UsersController.hasRole(req.user, 'utente', async function (decision) {

                

                if (!decision) {
                    return res.status(403).send(
                        { auth: false, token: null, message: 'You have no authorization.' });
                } else {
                    //vai buscar as receitas todas
                    const receitas = await Receita.find({});
                    const pacienteId = req.params["PacienteId"];
                    //console.log("PacienteId: " + pacienteId);

                    var receitasPaciente = new Array();

                    for (var i in receitas) {
                        if (pacienteId == receitas[i]["utente"]) {
                            const medicoAux = await User.findById(receitas[i].medico);
                            const utenteAux = await User.findById(receitas[i].utente);
                            receitas[i].nomeMedico = medicoAux.nome;
                            receitas[i].nomeUtente = utenteAux.nome;
                            receitasPaciente.push(receitas[i]);
                        }
                    }

                    console.log("Receitas do paciente")

                    res.status(200).json(receitasPaciente);
                }
            })
        }
        catch (err) {
            next(err);
        }
    },


    //Receitas Por Medico
    //router.route('/Receitas/:MedicoId')
    //.get(ReceitasController.getReceitasPorMedico)
    getReceitasPorMedico: async function (req, res, next) {
        try {
            console.log("router.route('/Receitas/:MedicoId')");
            console.log(".get(ReceitasController.getReceitasPorMedico)");

            UsersController.hasRole(req.user, 'medico', async function (decision) {

                if (!decision) {
                    return res.status(403).send(
                        { auth: false, token: null, message: 'You have no authorization.' });
                } else {
                    //vai buscar as receitas todas
                    const receitas = await Receita.find({});
                    const medicoId = req.params["MedicoId"];

                    var receitasMedico = new Array();

                    for (var i in receitas) {
                        if (medicoId == receitas[i]["medico"]) {
                            const medicoAux = await User.findById(receitas[i].medico);
                            const utenteAux = await User.findById(receitas[i].utente);
                            receitas[i].nomeMedico = medicoAux.nome;
                            receitas[i].nomeUtente = utenteAux.nome;
                            receitasMedico.push(receitas[i]);
                        }
                    }

                    res.status(200).json(receitasMedico);
                }
            })
        }
        catch (err) {
            next(err);
        }
    },


    //Receita para o Farmaceutico
    //router.route('/Receitas/:ReceitaId')
    //.get(VerifyToken,ReceitasController.getReceitaFarmaceutico)
    getReceitaFarmaceutico: async function (req, res, next) {
        try {
            console.log("router.route('/Receitas/:ReceitaId')");
            console.log(".get(VerifyToken,ReceitasController.getReceitaFarmaceutico)");

            UsersController.hasRole(req.user, 'farmaceutico', async function (decision) {

                if (!decision) {
                    return res.status(403).send(
                        { auth: false, token: null, message: 'You have no authorization.' });
                } else {
                    
                    const receitaId = req.params["ReceitaId"];
                    const receita = await Receita.findById(receitaId);

                    const medicoAux = await User.findById(receita.medico);
                    const utenteAux = await User.findById(receita.utente);
                    receita.nomeMedico = medicoAux.nome;
                    receita.nomeUtente = utenteAux.nome;

                    var receitasFarmaceutico = new Array();

                    receitasFarmaceutico.push(receita)

                    //console.log(receita)
                    res.status(200).json(receitasFarmaceutico);
                }
            })
        }
        catch (err) {
            next(err);
        }
    }
}