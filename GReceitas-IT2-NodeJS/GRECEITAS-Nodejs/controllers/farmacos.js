const Farmaco = require('../models/farmaco');
const User = require('../models/user');
var nodemailer = require('nodemailer');
const UsersController = require('../controllers/Users');


module.exports = {

    //Get all farmacos
    //router.route('/')
    //.get(FarmacosController.getFarmacos);
    getFarmacos: async function (req, res, next) {
        try {
            console.log("router.route('/')");
            console.log(".get(FarmacosController.getFarmacos)");
            console.log("req.path", req.path);

            const farmacos = await Farmaco.find({});

            res.status(200).json(farmacos);
        }
        catch (err) {
            next(err);
        }
    },

    //Get farmaco por id
    //router.route('/:FarmacoId')
    //.get(FarmacosController.getFarmacoById);
    getFarmacoById: async function (req, res, next) {
        try {
            console.log("router.route('/:FarmacoId')");
            console.log(".get(FarmacosController.getFarmacoById)");
            console.log("req.path", req.path);


            const farmacoId = req.params["FarmacoId"];
            const farmaco = await Farmaco.findById(farmacoId);
            res.status(200).json(farmaco);
        }
        catch (err) {
            next(err);
        }
    },



    //Post farmaco
    //router.route('/')
    //.post(FarmacosController.newFarmaco);

    newFarmaco: async function (req, res, next) {
        try {
            console.log("router.route('/')");
            console.log(".post(FarmacosController.newFarmaco)")

            const farmaco = new Farmaco(req.body);

            await farmaco.save();

            return res.status(201).json(farmaco);

        }
        catch (err) {
            next(err);
        }
    },


    //get comentarios de um farmaco
    //router.route('/:FarmacoId/comentarios')
    //.get(FarmacosController.getFarmacoComentariosById)
    getFarmacoComentariosById: async function (req, res, next) {
        try {
            console.log("router.route('/:FarmacoId/comentarios')");
            console.log(".get(FarmacosController.getFarmacoComentariosById)")

            const farmacoId = req.params["FarmacoId"];
            const newFarmaco = await Farmaco.findById(farmacoId);
            res.status(200).json(newFarmaco["comentarios"]);

        }
        catch (err) {
            next(err);
        }
    },


    //post new comentario para um farmaco
    //router.route('/:FarmacoId/comentarios')
    //.post(FarmacosController.newFarmacoComentarios);
    newFarmacoComentarios: async function (req, res, next) {
        try {
            console.log("router.route('/:FarmacoId/comentarios')");
            console.log(".post(FarmacosController.postFarmacoComentarios)");

            UsersController.hasRole(req.user, 'medico', async function (decision) {

                if (!decision) {
                    return res.status(403).send(
                        { auth: false, token: null, message: 'You have no authorization.' });
                } else {

                    const farmacoId = req.params["FarmacoId"];
                    const comentarioBody = req.body;
                    console.log("comentarioBody['medicoId']: " + comentarioBody['medicoId'])
                    const newFarmaco = await Farmaco.findById(farmacoId);
                    newFarmaco.comentarios.push(comentarioBody);
                    console.log("newFarmaco.comentarios: " + newFarmaco.comentarios)
                    const result = await newFarmaco.save();
                    res.status(200).json(newFarmaco);

                }

            });
        }
        catch (err) {
            next(err);
        }
    },



}    