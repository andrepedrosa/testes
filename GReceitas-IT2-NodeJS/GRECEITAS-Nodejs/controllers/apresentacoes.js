const Receita = require('../models/receita');
const Apresentacao = require('../models/apresentacao');
const Farmaco = require('../models/farmaco');
const Medicamento = require('../models/medicamento');
const User = require('../models/user');
var nodemailer = require('nodemailer');
const UsersController = require('../controllers/Users');


module.exports = {

    //Get all apresentacoes
    //router.route('/Apresentacoes')
    //.get(ReceitasController.getApresentacoes);
    getApresentacoes: async function (req, res, next) {
        try {
            console.log("router.route('/Apresentacoes')");
            console.log(".get(ApresentacoesController.getApresentacoes)");
            console.log("req.path", req.path);

            apresentacoes = await Apresentacao.find({});


            for (var i in apresentacoes) {

                const medicamentoAux = await Medicamento.findById(apresentacoes[i].medicamento);
                console.log("medicamentoAux: " + medicamentoAux)
                const farmacoAux = await Farmaco.findById(apresentacoes[i].farmaco);
                apresentacoes[i].nomeMedicamento = medicamentoAux.descricao;
                apresentacoes[i].nomeFarmaco = farmacoAux.descricao;

            }

            console.log(apresentacoes);
            res.status(200).json(apresentacoes);

        }
        catch (err) {
            next(err);
        }
    },


    //Get apresentacacoes por receita
    //router.route('/ReceitaId')
    //.get(ApresentacoesController.getApresentacoesById);
    getApresentacoesById: async function (req, res, next) {
        try {


            console.log("router.route('/ReceitaId')");
            console.log(".get(ApresentacoesController.getApresentacoesById)");

            const receitaId = req.params["ReceitaId"];
            const newreceita = await Receita.findById(receitaId);
            var allPrescricoes = new Array();

            const prescricoes = newreceita["prescricoes"];
            for (var j = 0; j < prescricoes.length; j++) {
                allPrescricoes.push(prescricoes[j]);
            }

            var allApresentacoes = new Array();

            for (var j = 0; j < allPrescricoes.length; j++) {
                allApresentacoes.push(allPrescricoes[j]["apresentacao"]);
            }


            res.status(200).json(allApresentacoes);

        } catch (err) {
            next(err);
        }
    },

    //new apresentacao
    //router.route('/')
    //.post(ApresentacoesController.newApresentacao);
    newApresentacao: async function (req, res, next) {
        try {
            console.log("router.route('/')");
            console.log(".post(ApresentacoesController.newApresentacao)");


            const apresentacao = new Apresentacao(req.body);

            await apresentacao.save();

            return res.status(201).json(apresentacao);


        }
        catch (err) {
            next(err);
        }
    },


    //get apresentacoes clientes
    //router.route('/pacientes/:pacienteId')
    //.get(ApresentacoesController.getApresentacoesPacientes);
    getApresentacoesPacientes: async function (req, res, next) {
        try {
            console.log("router.route('/Apresentacoes')");
            console.log(".get(ApresentacoesController.getApresentacoes)");
            console.log("req.path", req.path);

            UsersController.hasRole(req.user, 'utente', async function (decision) {


                if (!decision) {
                    return res.status(403).send(
                        { auth: false, token: null, message: 'You have no authorization.' });
                } else {


                    //buscar as receitas
                    const receitas = await Receita.find({});

                    //buscar o id do paciente
                    const pacienteId = req.params["PacienteId"];

                    //receitas todas do paciente
                    var apresentacoesPaciente = new Array();
                    for (var i in receitas) {
                        if (pacienteId == receitas[i]["utente"]) {
                            const prescricoes = receitas[i]["prescricoes"];
                            for (var z = 0; z < prescricoes.length; z++) {
                                const apresentacaoAux = prescricoes[z].apresentacao;
                                apresentacoesPaciente.push(apresentacaoAux)
                            }
                        }
                    }

                    console.log(apresentacoesPaciente);
                    res.status(200).json(apresentacoesPaciente);

                }
            })
        }
        catch (err) {
            next(err);
        }
    },

}