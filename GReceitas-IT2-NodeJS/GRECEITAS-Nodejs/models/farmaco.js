const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const farmacoSchema = new Schema({
    descricao: { type: String,  unique: true },
    comentarios : [{
        medico: {
            type: Schema.Types.ObjectId,
            ref: 'user',
        },
        comentario: String
    }]
});

const Farmaco = mongoose.model('farmaco',farmacoSchema);
module.exports = Farmaco;