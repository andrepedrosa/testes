const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const receitaSchema = new Schema({
    emitida: { type: Boolean, default: false },
    data : { type : Date, default: Date.now },
    utente: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    medico: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    nomeMedico: String,
    nomeUtente: String,
    prescricoes : [{
        apresentacao : {
            type: Schema.Types.ObjectId,
            ref: 'apresentacao',
            required: true
        },
        dataValidade : Date,
        quantidade : Number,
        aviamentos : [{
            farmaceutico: {
                type: Schema.Types.ObjectId,
                ref: 'user',
                required: true
            },
            dataAviamento : Date,
            quantidade : Number
        }]
    }]

});

const Receita = mongoose.model('receita',receitaSchema);
module.exports = Receita;