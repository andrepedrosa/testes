const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const apresentacaoSchema = new Schema({
    medicamento: {
        type: Schema.Types.ObjectId,
        ref: 'medicamento',
        required: true 
    },
    farmaco: {
        type: Schema.Types.ObjectId,
        ref: 'farmaco',
        required: true 
    },
    posologia: String,
    dosagem: String,
    formato: String,
    nomeMedicamento: String,
    nomeFarmaco: String
});

const Apresentacao = mongoose.model('apresentacao',apresentacaoSchema);
module.exports = Apresentacao;