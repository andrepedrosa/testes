const express = require('express');
const router = express.Router();
const ReceitasController = require('../controllers/Receitas');
const VerifyToken = require('../auth/VerifyToken');


router.route('/')
.get(VerifyToken, ReceitasController.index)
.post(ReceitasController.newReceita);


router.route('/:ReceitaId')
.get(ReceitasController.getReceitaById)
.put(ReceitasController.replaceReceitaById)
.delete(ReceitasController.delReceita);

router.route('/:ReceitaId/Prescricao')
.post(ReceitasController.newPrescricao)
.get(ReceitasController.getPrescricoes);

router.route('/:ReceitaId/Prescricao/:PrescricaoId')
.put(ReceitasController.replacePrescricao)
.delete(ReceitasController.delPrescricao);

router.route('/:ReceitaId/Prescricao/:PrescricaoId/Aviamento')
.post(VerifyToken,ReceitasController.newAviamento) //prova de conceito da autenticacao: so aceita um farmaceutico autenticado
.get(ReceitasController.getAviamentos);

router.route('/Prescricoes/')
.get(ReceitasController.getAllPrescricoes);

router.route('/Aviamentos/:AviamentoId')
.get(ReceitasController.getAviamentoById);

router.route('/Prescricoes/:PrescricaoId')
.get(ReceitasController.getPrescricaoById);

// router.route('/Medico/:MedicoId')
// .get(ReceitasController.getReceitasMedico);

// router.route('/Utente/:UtenteId')
// .get(ReceitasController.getReceitasUtente);

router.route('/:ReceitaId/Emitir')
.put(VerifyToken,ReceitasController.emitirReceita);
 
router.route('/Utente/:UtenteId/Prescricao/Poraviar/:data?')
.get(ReceitasController.prescricoesPorAviar); //prova de conceito da autenticacao: so aceita um medico autenticado


module.exports = router;