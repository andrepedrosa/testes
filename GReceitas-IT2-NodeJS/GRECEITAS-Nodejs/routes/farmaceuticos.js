const express = require('express');
const router = express.Router();
const ReceitasController = require('../controllers/receitas');
const VerifyToken = require('../auth/VerifyToken');

router.route('/Receitas/:ReceitaId')
.get(VerifyToken,ReceitasController.getReceitaFarmaceutico)

module.exports = router;