const express = require('express');
const router = express.Router();
const ReceitasController = require('../controllers/receitas');
const VerifyToken = require('../auth/VerifyToken');

router.route('/Receitas/:PacienteId')
.get(VerifyToken,ReceitasController.getReceitasPorPaciente)

module.exports = router;