const express = require('express');
const router = express.Router();
const ApresentacoesController = require('../controllers/apresentacoes');
const VerifyToken = require('../auth/VerifyToken');

router.route('/')
.get(ApresentacoesController.getApresentacoes)
.post(ApresentacoesController.newApresentacao);

router.route('/:ApresentacaoId')
.get(ApresentacoesController.getApresentacoesById);


router.route('/pacientes/:PacienteId')
.get(VerifyToken,ApresentacoesController.getApresentacoesPacientes);

module.exports = router;