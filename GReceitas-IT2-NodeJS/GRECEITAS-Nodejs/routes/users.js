const express = require('express');
const router = express.Router();
const { validateBody, schemas } = require('../helpers/routeHelpers');
const UsersController = require('../controllers/Users');
const VerifyToken = require('../auth/VerifyToken');

router.route('/')
    .get(UsersController.index);

router.route('/signup')
    .post(UsersController.signUp);

router.route('/login')
    .post(UsersController.login);

router.route('/secret')
    .get(VerifyToken,UsersController.secret);



module.exports = router;