const express = require('express');
const router = express.Router();
const FarmacosController = require('../controllers/farmacos');
const VerifyToken = require('../auth/VerifyToken');

router.route('/')
.get(FarmacosController.getFarmacos)
.post(FarmacosController.newFarmaco);

router.route('/:FarmacoId')
.get(FarmacosController.getFarmacoById);

router.route('/:FarmacoId/comentarios')
.get(FarmacosController.getFarmacoComentariosById)
.post(VerifyToken,FarmacosController.newFarmacoComentarios);

module.exports = router;