const express = require('express');
const router = express.Router();
const ReceitasController = require('../controllers/receitas');
const VerifyToken = require('../auth/VerifyToken');

router.route('/Receitas/:MedicoId')
.get(VerifyToken,ReceitasController.getReceitasPorMedico)

module.exports = router;