const express = require('express');
const router = express.Router();
const MedicamentosController = require('../controllers/medicamentos');
const VerifyToken = require('../auth/VerifyToken');

router.route('/')
.get(MedicamentosController.getMedicamentos)
.post(MedicamentosController.newMedicamento);

router.route('/:MedicamentoId')
.get(MedicamentosController.getMedicamentoById);



module.exports = router;